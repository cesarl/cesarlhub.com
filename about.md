---
layout: page
title: "About"
weight: 4
no-title: true
type: txt
description: ""
---
{% include JB/setup %}

Je m'appelle César Leblic, je vis à Paris. Je suis développeur web et logiciel.

J'ai développé sur Drupal (6 - 7) pendant près de 3 ans. Je me suis ensuite spécialisé dans le developpement front-end (Javascript / HTML5).
Je suis co-gérant d'Activefab, un jeune studio de developpement web (création en 2010), ainsi qu'étudiant à Epitech depuis mars 2012.

Vous pouvez consulter mon portfolio dans lequel je liste mes principales réalisations. Ici mon cv. Vous pouvez aussi (pour le lol) consulter mon cv animé.

Je suis toujours à la recherche de projets innovants, créatifs, amusants, artistiques, militants. Donc n'hésitez pas à me contacter si vous avez envi de travailler avec moi ! Même si vous êtes jeunes et fauchés !

Remarque : J'ai fait ce site pour m'amuser à y partager mes dernières expériences en terme de développement. Si le code est cradi-crado ou buggy ne vous étonnez donc pas, c'est du code souvent écrit très rapidement pendant mon temps libre pour m'amuser un peu :). Idem pour certaines parties du site ; en bref ici c'est mon univers sans clients, et sans internet explorer ou je peux coder des trucs crados en toute liberté et illustrer mes articles avec des photos de chaton ! Vous serez prévenus héhé

### update

J'ai transféré en septembre 2012 ce site d'un Drupal 7 à un Jekyll (soulagement). En transférant les contenus je me suis rendu compte que le javascript des posts de 2011 est vraiment horrible. Si vous êtes débutant (comme je l'étais lorsque je les ai rédigé) ne vous fiez pas totallement à ce qui est écrit / dit / codé ;D
