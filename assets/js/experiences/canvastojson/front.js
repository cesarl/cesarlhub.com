(function ($) {
	Drupal.behaviors.cesarlc = {};
	Drupal.behaviors.cesarlc.attach = function(context) {



	var width = 930;
	var height = 223;
	
	var canvasBg = document.getElementById("canvas-bg");
	canvasBg.width = width;
	canvasBg.height = height;
	
	var canvasDraw = document.getElementById("canvas-draw");
	canvasDraw.width = width;
	canvasDraw.height = height;

	var bgctx = canvasBg.getContext('2d');
	var ctx = canvasDraw.getContext('2d');
	
	var lastPos = {x:0, y:0};
	var mousePos = {x:0, y:0};

	var ink = false;
	var colorLine = $(".couleur.active").attr("color");
	var lineWidth = 10;
		
	var strike = [];
	var path = [];
	var hasMove = false;

	var positionTracker = document.getElementById("position-tracker");
	var surface2Travail = document.getElementById("surfacedetravail");
	
	var block1 = document.getElementById("main")

	var ucandraw = false;

	bgctx.fillStyle = "#FFF";
	bgctx.rect(0,0,width,height);
	bgctx.fill();

	positionTracker.addEventListener("mousedown", on_mousedown, false);
	positionTracker.addEventListener("mousemove", on_mousemove, false);
	positionTracker.addEventListener("mouseup", on_mouseup, false);
	
	
	$("#toolbar .couleur").click(function(){
		colorLine = $(this).attr("color");
		$("#toolbar .couleur").not(this).removeClass("active");
		$(this).addClass("active");
	});
	$("#toolbar .erase").click(function(){
		bgctx.rect(0,0,width,height);
		bgctx.fill();
		ctx.clearRect(0,0,width,height);
		localStorage.removeItem('jsonDraw');
	});
	
	
	function scrollPosition(e) {
			return {
					x: document.scrollLeft || window.pageXOffset,
					y: document.scrollTop || window.pageYOffset
			};
	}
	function calculateMouseCoord(e){
		return{
			x: e.clientX - surface2Travail.offsetLeft - block1.offsetLeft + scrollPosition().x,
			y: e.clientY - surface2Travail.offsetTop - block1.offsetTop + scrollPosition().y
		}
	}
	function on_mousedown(e){
		if(ucandraw == false){
			ucandraw = true;
		}
		ink = true;
		lastPos = calculateMouseCoord(e);
		path.push({color:colorLine});
	}
	function on_mousemove(e){
		if(ink == false){return false} 
		else{
			hasMove = true;
			mousePos = calculateMouseCoord(e);
			circleRadius = Math.abs(lastPos.x - mousePos.x);
			drawOnMyCanvas(e);
		}
	}
	function on_mouseup(){
		ink = false;
		if(hasMove == true){
			strike.push({nbr:path});
			console.log(strike);
			path = [];
			toJson();
		}
	}
	
	function drawOnMyCanvas(){
		ctx.beginPath();
		ctx.strokeStyle = colorLine;
		ctx.lineWidth = lineWidth;
		ctx.lineCap = 'round';
		ctx.lineJoin = 'round';
		ctx.moveTo(lastPos.x, lastPos.y);
		ctx.lineTo(mousePos.x, mousePos.y);
		ctx.stroke();
		ctx.closePath();
		path.push({line:{x:lastPos.x, y:lastPos.y, toX:mousePos.x, toY:mousePos.y}})
		lastPos.x = mousePos.x;
		lastPos.y = mousePos.y;
	}


	function toJson(){
		var o = '{';
		o += '"value":[';
		for(var l=0; l<strike.length; l++){
			o += '{"ligne":[';
			o += '{"color":"'+ strike[l].nbr[0].color +'"},'
			for(var n=1; n<strike[l].nbr.length; n++){
				o += '{'
				o += '"x":' + strike[l].nbr[n].line.x + ',';
				o += '"y":' + strike[l].nbr[n].line.y + ',';
				o += '"toX":' + strike[l].nbr[n].line.toX + ',';
				o += '"toY":' + strike[l].nbr[n].line.toY;
				o += '}';
				if(n != strike[l].nbr.length -1){
					o += ',';
				}
			}
			o += (']}')
			if(l != strike.length -1){
				o += ','
			}
		}
		o += ']}'
			
		//$("#textarea").empty().text(o);
		
		localStorage['jsonDraw'] = o.toString();
	}


	var mydraw = localStorage.getItem("jsonDraw");
	var localornot;
	if (mydraw != null){
		localornot = true;
		parsemydraw(JSON.parse(mydraw))
	} else {
		localornot = false;
		var getitnow = new XMLHttpRequest();
		getitnow.overrideMimeType("application/json");
		getitnow.open('GET', 'http://www.cesar.lc/sites/www.cesar.lc/themes/cesarlc/experiences/canvastojson/default.json', true);
		getitnow.onreadystatechange = function () {
			if (getitnow.readyState == 4) {
				var jsonTexto = JSON.parse(getitnow.responseText);
				parsemydraw(jsonTexto);
			}
		}
		getitnow.send(null);
	}
	
	function parsemydraw(data){
		var result = data;
		var p=0;
		var q = 0;
		
		drawJson()
		
		function drawJson(){
			bgctx.strokeStyle = colorLine;
			bgctx.lineWidth = lineWidth;
			bgctx.lineCap = 'round';
			bgctx.lineJoin = 'round';
			if(p<result.value.length){
				if(q<result.value[p].ligne.length){
					bgctx.strokeStyle = result.value[p].ligne[0].color;
					bgctx.beginPath();
					bgctx.moveTo(result.value[p].ligne[q].x, result.value[p].ligne[q].y)
					bgctx.lineTo(result.value[p].ligne[q].toX, result.value[p].ligne[q].toY);
					bgctx.stroke();
					bgctx.closePath();
					q++
					if(ucandraw == false){setTimeout(drawJson,10)}
					else{
						bgctx.rect(0,0,width,height);
						bgctx.fill();
					}
				} else{
					q = 0;
					p++
					setTimeout(drawJson,10);
				}
			} else {
				ucandraw = true;
				return false
			}
		}
	}



	}
})(jQuery);
