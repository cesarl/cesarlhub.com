(function ($) {
	Drupal.behaviors.cesarlc = {};
	Drupal.behaviors.cesarlc.attach = function(context) {
	var width = 428;
	var height = 320;
	var bobsWorld = new Image();
	bobsWorld.src = "/sites/www.cesar.lc/themes/cesarlc/experiences/greenscreen/files/green.png";
	
	var bobsDream = new Image();
	bobsDream.src = "/sites/www.cesar.lc/themes/cesarlc/experiences/greenscreen/files/dog.png";
	
	bobsDream.addEventListener('load', launchApp, false);

	
	var bobGreen = document.getElementById("canvas");
	bobGreen.width = width;
	bobGreen.height = height;
	var ctx = bobGreen.getContext("2d");
	

	
	function launchApp(){
		ctx.drawImage(bobsWorld, 0,0);

		var rot = 0;
		var mat = 0;
		var dif = 0.01;
		var pixelArray = ctx.getImageData(0,0,width,height);
		

		for(i=0, max = pixelArray.data.length; i<max; i++){
			if(pixelArray.data[i] >= 10 && pixelArray.data[i+1] >= 10 && pixelArray.data[i+2] <= 3){
				pixelArray.data[i+3] = 0;
			}
		}
		
		ctx.putImageData(pixelArray,0,0);
		ctx.fillStyle = "rgb(255,0,0)";
		ctx.arc(204,123,15,0,360*Math.PI/180,false);
		ctx.fill();
	
		var bobTransparent = new Image();
		bobTransparent.src = bobGreen.toDataURL();
	
		setInterval(andBobWillBeHappyForEver,33)
		
		function andBobWillBeHappyForEver(){
			rot++
			mat = mat+dif;
//			ctx.clearRect(0,0,width,height)
			ctx.drawImage(bobsDream,0,0);

//dont work on firefox 5.0.1
/*
			ctx.shadowColor = "rgba(0,0,0,0.4)";
			ctx.shadowOffsetX = ctx.shadowOffsetY = 30;
			ctx.shadowBlur = 20;
*/

			ctx.save();
			ctx.translate(width/2,height/2);
			ctx.rotate(rot*Math.PI/180);
			ctx.transform(1,-mat,-mat,1,0,0)
			ctx.translate(-width/2,-height/2);
			ctx.drawImage(bobTransparent,0,0);
			ctx.restore();
			if (rot == 360){rot = 0};
			if (mat >= 1){dif = -0.01}
			if(mat <= 0){dif = 0.01}
		}
	}


	}
})(jQuery);