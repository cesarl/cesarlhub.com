	var MIN_WIDTH = 960;
	var MIN_HEIGHT = 700;
	var windowSize = {
		width: 0,
		height: 0
	};
windowSize.width = Math.max( window.innerWidth, MIN_WIDTH );
windowSize.height = Math.max( window.innerHeight, MIN_HEIGHT );

function transform_event_coord(e) {
  var blockPosX = ($("#block-block-1").offset()).left;
  var blockPosY = ($("#block-block-1").offset()).top;
  return {x: e.clientX - (windowSize.width - canvas.width) * 0.5 + scrollPosition().x, y:e.clientY - (windowSize.height - canvas.height) * 0.5 + scrollPosition().y};
//  return {x: e.clientX - blockPosX, y: e.clientY - blockPosY};
}
function scrollPosition() {
		var blockPosY = ($("#block-block-1").offset()).top;
		return {
			x: document.body.scrollLeft || window.pageXOffset + 8,
			y: ( document.body.scrollTop || window.pageYOffset ) + blockPosY - 24
		};
	}
$(document).bind('scroll', function(e){
  lastpos = transform_event_coord(e);
})
//default boolean bloquant l'encre
var drawing = false;

//position du curseur par d�faut en dehors du canvas
var lastpos = {x:-1,y:-1};

//fonction on mouse down
function on_mousedown(e) {
  //active l'encre
  drawing = true;
  //redefinie la position du stylo � l'endroit du curseur, si comment�e, alors chaque nouvau clique reprendra du dernier mouse up
  lastpos = transform_event_coord(e);
}

//fonction on mouse move
function on_mousemove(e) {

  //si drawing est false, alors rien ne se passe
  if (!drawing)
    return;
  
  //position du curseur
  var pos = transform_event_coord(e);

  //get canvas
  var canvas = document.getElementById("canvas");
  
  //launch le canvas
  var ctx = canvas.getContext("2d");

  //couleur de l'encre
  ctx.strokeStyle = "rgb(226,10,108)";
  
  //largeur de l'encre
  ctx.lineWidth = 1.0;

  //pour un trac� plus style pastel
  //ctx.lineJoin = "round";
  ctx.lineCap = "round";

  //d�but du trac�
  ctx.beginPath();
  
  //positionner le curseur � la derniere poistion du curseur mousedown
  ctx.moveTo(lastpos.x, lastpos.y);

  //trac� jusqu'a la position actuelle du curseur mousedown
  ctx.lineTo(pos.x, pos.y);
  
  //fermeture du trac�
  ctx.closePath();
  
  //application du contour du trac�
  ctx.stroke();
  ctx.fill();
  
  //la derni�re position devient la position actuelle
  lastpos = pos;
}

//initialise CloudR
function init() {

  var container = $("#recipient");
  container.append('<canvas id="canvas"/>')
  var canvas = document.getElementById("canvas");
  var ctx = canvas.getContext("2d");
//  var SCREEN_WIDTH = window.innerWidth;
//  var SCREEN_HEIGHT = window.innerHeight;
  canvas.width = 930;
  canvas.height = 250;


  addEventListener("mousedown", on_mousedown, false);
  addEventListener("mousemove", on_mousemove, false);
    draw(ctx);
}

init();


    function draw(ctx) {

      // dsqd/Trac
      ctx.save();
      ctx.beginPath();
      ctx.moveTo(452.4, 23.9);
      ctx.bezierCurveTo(439.9, 24.9, 440.4, 38.9, 443.9, 47.9);
      ctx.bezierCurveTo(445.4, 51.4, 450.4, 55.9, 449.9, 59.9);
      ctx.bezierCurveTo(449.4, 68.9, 430.9, 77.4, 424.4, 80.9);
      ctx.strokeStyle = "rgb(157, 0, 92)";
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(422.9, 46.9);
      ctx.bezierCurveTo(421.9, 50.9, 419.9, 64.9, 423.9, 67.4);
      ctx.bezierCurveTo(434.4, 73.9, 439.4, 39.4, 439.9, 35.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(413.9, 40.9);
      ctx.bezierCurveTo(413.9, 29.9, 398.9, 33.9, 407.4, 45.4);
      ctx.bezierCurveTo(411.9, 51.9, 415.4, 49.4, 414.4, 60.9);
      ctx.bezierCurveTo(412.9, 71.4, 404.4, 72.9, 396.4, 80.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(385.4, 42.4);
      ctx.bezierCurveTo(376.9, 33.4, 370.9, 39.4, 384.4, 47.4);
      ctx.bezierCurveTo(390.4, 50.9, 395.9, 51.9, 399.4, 58.4);
      ctx.bezierCurveTo(404.9, 69.4, 397.4, 73.4, 387.4, 75.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(360.9, 54.4);
      ctx.bezierCurveTo(367.9, 57.9, 370.9, 37.9, 362.9, 42.4);
      ctx.bezierCurveTo(359.9, 44.4, 357.4, 51.9, 357.9, 54.9);
      ctx.bezierCurveTo(359.9, 62.9, 374.9, 65.9, 381.4, 66.9);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(351.9, 53.4);
      ctx.bezierCurveTo(349.4, 38.4, 332.9, 40.4, 337.4, 57.9);
      ctx.bezierCurveTo(338.4, 62.4, 340.9, 70.9, 345.9, 68.9);
      ctx.bezierCurveTo(351.4, 66.4, 350.9, 43.4, 351.4, 39.9);
      ctx.bezierCurveTo(352.4, 26.4, 352.9, 13.4, 353.4, 0.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(316.4, 21.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(313.9, 71.9);
      ctx.bezierCurveTo(312.9, 58.9, 316.4, 45.4, 316.9, 32.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(304.4, 62.9);
      ctx.bezierCurveTo(304.9, 56.4, 307.4, 46.9, 303.9, 41.4);
      ctx.bezierCurveTo(295.9, 44.4, 280.4, 65.4, 287.9, 74.4);
      ctx.bezierCurveTo(297.9, 85.9, 310.9, 48.9, 309.4, 40.9);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(243.9, 81.9);
      ctx.bezierCurveTo(244.4, 63.9, 246.9, 46.4, 249.4, 28.4);
      ctx.bezierCurveTo(248.9, 36.9, 247.4, 48.4, 251.4, 56.9);
      ctx.bezierCurveTo(259.9, 44.4, 263.9, 23.4, 263.4, 8.9);
      ctx.bezierCurveTo(261.9, 29.9, 272.9, 52.4, 276.9, 73.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(200.4, 67.4);
      ctx.bezierCurveTo(206.9, 66.9, 218.4, 63.9, 221.9, 57.9);
      ctx.bezierCurveTo(228.4, 46.9, 208.4, 44.4, 200.9, 48.4);
      ctx.bezierCurveTo(187.9, 55.9, 194.4, 72.4, 206.9, 77.9);
      ctx.bezierCurveTo(217.9, 82.4, 220.9, 72.4, 226.9, 65.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(172.4, 73.9);
      ctx.bezierCurveTo(177.9, 66.9, 178.9, 56.9, 183.9, 49.9);
      ctx.bezierCurveTo(184.9, 57.9, 182.9, 68.4, 185.4, 75.9);
      ctx.bezierCurveTo(191.4, 63.9, 195.4, 50.9, 196.9, 37.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(151.9, 72.4);
      ctx.bezierCurveTo(151.9, 61.9, 155.9, 51.9, 156.4, 41.4);
      ctx.bezierCurveTo(154.9, 50.9, 152.9, 68.4, 162.9, 74.4);
      ctx.bezierCurveTo(172.4, 66.9, 171.4, 47.4, 174.4, 36.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(134.4, 39.4);
      ctx.bezierCurveTo(133.4, 37.9, 132.4, 36.9, 131.4, 36.4);
      ctx.bezierCurveTo(129.4, 39.4, 129.9, 40.4, 131.4, 41.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(123.4, 64.4);
      ctx.bezierCurveTo(130.4, 65.4, 132.4, 54.9, 132.9, 49.9);
      ctx.bezierCurveTo(131.4, 55.4, 137.9, 80.9, 145.4, 69.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(93.9, 70.4);
      ctx.bezierCurveTo(97.4, 65.9, 102.9, 56.9, 99.9, 51.4);
      ctx.bezierCurveTo(105.4, 52.9, 124.4, 66.4, 124.9, 72.4);
      ctx.bezierCurveTo(125.9, 79.4, 112.9, 83.9, 106.9, 85.9);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(47.4, 77.9);
      ctx.bezierCurveTo(54.4, 77.9, 68.4, 67.9, 67.4, 59.9);
      ctx.bezierCurveTo(65.9, 44.4, 38.9, 51.4, 34.4, 60.9);
      ctx.bezierCurveTo(27.4, 75.4, 45.4, 82.4, 56.4, 80.9);
      ctx.bezierCurveTo(72.4, 78.9, 75.9, 65.4, 78.4, 51.9);
      ctx.bezierCurveTo(95.9, 53.9, 95.9, 81.4, 78.9, 85.4);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(4.4, 51.4);
      ctx.bezierCurveTo(4.4, 41.4, 1.9, 29.4, 5.4, 19.9);
      ctx.bezierCurveTo(10.9, 3.9, 29.4, 6.9, 39.9, 16.4);
      ctx.bezierCurveTo(53.9, 28.9, 51.4, 54.4, 40.4, 68.4);
      ctx.bezierCurveTo(35.4, 75.4, 11.4, 102.4, 5.9, 84.9);
      ctx.stroke();

      // dsqd/Trac
      ctx.beginPath();
      ctx.moveTo(21.4, 36.9);
      ctx.bezierCurveTo(19.4, 31.4, 16.9, 32.9, 13.4, 34.9);
      ctx.bezierCurveTo(4.9, 39.9, 5.4, 56.9, 6.4, 64.4);
      ctx.bezierCurveTo(7.9, 73.4, 13.9, 83.9, 0.9, 83.4);
      ctx.stroke();
      ctx.restore();
      
    }
