(function ($) {
	Drupal.behaviors.cesarlc = {};
	Drupal.behaviors.cesarlc.attach = function(context) {
	
	velWidth = 177;
	velHeight = 146;
	var velo = new Image();
	velo.src = "/sites/www.cesar.lc/themes/cesarlc/experiences/moncv/files/totaltile.png";
	velo.addEventListener('load', loader, false);
	var personnage = document.getElementById("personnage");
	personnage.width = velWidth;
	personnage.height = velHeight;
	
	function loader(){
		$("#header, .header-bg").slideUp();
		$(".loader").delay(2000).fadeOut(800, launchApp())
	}
	
	var dx=177;
	var dy=146;
	var x=0;
	var y=0;
	var disTot = 3000;
	var disPlus = 20;
	var direction;
	var plusLeft = 0;
	var plusRight = 0;
	var disParc = plusRight - plusLeft;

	function launchApp(){
		ctx = personnage.getContext("2d");
		ctx.drawImage(velo, 0,0);
		launchNav();
	}
	
	$(".stbk *").each(function(){
		if ($(this).attr("pos") >= 0){
			$(this).hide();
		}
	});
	
	
	function launchNav(){
	
	document.addEventListener('mousemove', onmousemove, false);
	
	function onmousemove(e){
//		console.log(e.clientY + $(document).scrollTop())
		if(e.clientY + $(document).scrollTop() < 100 && e.clientY + $(document).scrollTop() > 0){
			$("#header, .header-bg").slideDown();
		} else {
			$("#header, .header-bg").slideUp();
		}
	}
	
	var yucandraw = true;
	var scrollY = 0;
	$(".crop-tracker").scroll(function(){
	var ancienScroll = scrollY;
	scrollY = $(".crop-tracker").scrollTop();
	if(scrollY - ancienScroll > 0){
				y=0;
				x+=dx;
				if (x>=1416-dx){
					x=0;
				}
				direction = "right";
				disParc = scrollY;
	}
	if(scrollY - ancienScroll < 0){
				y=dy;
				x+=dx;
				if (x>=1416-dx){
					x=0;
				}
				direction = "left";
				disParc = scrollY;
	}
			$(".story-slide").scrollLeft(scrollY)
//			console.log(scrollY);
			if(disParc >= 3258 && disParc < 5240){
				y+= 292;
			}
			if(disParc >= 5240){
				y+= 584;
			}
			drawScreen();
	$(".stbk *").each(function(){
		if ($(this).attr("pos") >= disParc){
			$(this).fadeOut(100);
		} else {$(this).fadeIn(100)}
	});
});

	function drawScreen() {
			ctx.clearRect(0,0,dx,dy);
			ctx.drawImage(velo, x, y,dx,dy,0,0,dx,dy);
	}


	}
	}
})(jQuery);
