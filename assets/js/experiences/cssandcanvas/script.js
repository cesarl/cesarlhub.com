(function ($) {
	Drupal.behaviors.cesarlc = {};
	Drupal.behaviors.cesarlc.attach = function(context) {
	
	
	function cssVsCanvas(){
		var selectorCss = $("#recipient h1");
		var mousePosx;
		var mousePosy;
		var elementW;
		var elementH;
		var coefHor;
		var coefVer;

			drawCss();
			drawScreen();
			

		$(window).mousemove(function(e){
			mousePosx = e.pageX;
			mousePosy = e.pageY;
			elementW = $(this).width();
			elementH = $(this).height();
			coefHor = - (mousePosx - elementW/2)/30;
			coefVer = - (mousePosy - elementH/2)/30;
			coefHor = Math.round(coefHor);
			coefVer = Math.round(coefVer);
			drawCss();
			drawScreen();
		});

		function drawCss(){
			var theShad = coefHor + "px " + coefVer+ "px 16px rgba(0,0,0,0.3)";
			$(selectorCss).css({textShadow: theShad});
		}

		function drawScreen() {
			var canvas = document.getElementById("my_canvas");
			var ctx = canvas.getContext("2d");
			ctx.clearRect(0,0,450,179);
			ctx.shadowColor = "rgba(0,0,0,0.5)";
			ctx.shadowOffsetX = coefHor;
			ctx.shadowOffsetY = coefVer;
			ctx.shadowBlur = 16;
			ctx.textBaseline = "middle";
			ctx.fillStyle = "#29A7DE";
			ctx.font = "109px Carter One";
			ctx.fillText(" canvas", 0, 80);
		}
	}
	cssVsCanvas();
	}
})(jQuery);