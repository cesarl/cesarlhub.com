	window.addEventListener('load', canvasApplication, false);
	function canvasApplication(){
		
		
		//block selection
		document.onselectstart = function () { return false; }
		
		//proprietes
		
		var fillOp
		var strokeOp
		var fillCol
		var strokeCol
		var colorLine
		var colorFill
		var lineWidth
		var miniCanvas = document.getElementById("apercu-proprietes");
		var minictx = miniCanvas.getContext('2d');
		var miniWidth = miniCanvas.width;
		var miniHeight = miniCanvas.height;
		var isStroke;
		var isFill;
			
		propertiesPlease();
		
		function propertiesPlease(){
			fillOp = $("#opacity-fill").val()/100;
			strokeOp = $("#opacity-stroke").val()/100;
			lineWidth = Number($("#stroke-width").val())
			fillCol = document.getElementById("fill-color");
			strokeCol = document.getElementById("stroke-color");
			colorFill = "rgba("+Math.round(fillCol.color.rgb[0]*255)+","+Math.round(fillCol.color.rgb[1]*255)+","+Math.round(fillCol.color.rgb[2]*255)+","+fillOp+")";
			colorLine = "rgba("+Math.round(strokeCol.color.rgb[0]*255)+","+Math.round(strokeCol.color.rgb[1]*255)+","+Math.round(strokeCol.color.rgb[2]*255)+","+strokeOp+")";
			isStroke = $("#stroke").attr("checked");
			isFill = $("#fill").attr("checked");
			
			
			minictx.strokeStyle = colorLine;
			minictx.lineWidth = lineWidth;
			minictx.fillStyle = colorFill;
			minictx.clearRect(0,0,miniWidth,miniHeight);
			if(isFill === "checked"){
				minictx.beginPath();
				minictx.rect(70,40, 60,60);
				minictx.fill();
				minictx.closePath();
				$("#fill-box").show();
			} else {$("#fill-box").hide()}
			if(isStroke === "checked"){
				minictx.beginPath();
				minictx.rect(70-(lineWidth*0.5),40-(lineWidth*0.5), 60+lineWidth,60+lineWidth);
				minictx.stroke();
				minictx.closePath();
				$("#stroke-box").show();
			} else {$("#stroke-box").hide();}
		}
		
		$("#proprietes input").change(function(e){
			propertiesPlease();
		});
	
		var width = 680;
		var height = 656;
		var canvas = document.getElementById("canvas");
		canvas.width = width;
		canvas.height = height;
		var ctx = canvas.getContext('2d');
		var lastPos = {x:0, y:0};
		var mousePos = {x:0, y:0};
		var mouseDiff = {x:0, y:0};
		var ink = false;
		var myImage = new Image();
		myImage.src = "/assets/js/experiences/drawing_board/images/big-squirrel.jpg";
		myImage.addEventListener('load', imageIsLoaded, false);
		var letterPressed;
		var touchPressed;
		var sauvegarde = [];
		var outil = "pinceau";
		var action = "";
		var distance = {x:10, y:10};
		var lastCircleRadius;
		$("#toolbar").find("."+outil).addClass("active");
		
		function imageIsLoaded(){
			ctx.drawImage(myImage, 0,0);

			canvas.addEventListener("mousedown", on_mousedown, false);
			canvas.addEventListener("mousemove", on_mousemove, false);
			canvas.addEventListener("mouseup", on_mouseup, false);
			window.addEventListener("keyup",oneKeyUp,true);
			window.addEventListener("keydown",oneKeyDown,true);

		$("#toolbar .bouton").click(function(){
			if($(this).hasClass("tool")){
				$(this).addClass("active")
				$(".tool").not(this).removeClass("active");
				outil = $(this).attr("toolstype");
			};
			if($(this).hasClass("action")){
				action = $(this).attr("toolstype");
				actions();
			}
		});
		}
		function oneKeyUp(e){
			letterPressed = String.fromCharCode(e.keyCode);
			touchPressed = "";
			actions(e);
		}
		function oneKeyDown(e){
			touchPressed = e.keyCode;
		}
		function actions(){
			if(action === "historique" || letterPressed === "Z"){
				var i = Number(sauvegarde.length);
				if(i>0){
					ctx.putImageData(sauvegarde.pop(), 0, 0);
				}
			}
		
			if(action === "sauvegarde" || letterPressed === "S"){
				var canvasToImg = canvas.toDataURL();
				window.open(canvasToImg,"nom de la fenêtre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
			}
			action ="";
			letterPressed = "";
		}

		function scrollPosition(e) {
			return {
				x: document.scrollLeft || window.pageXOffset,
				y: document.scrollTop || window.pageYOffset
			};
		}
		function calculateMouseCoord(e){
			return{
				x: e.clientX - canvas.offsetLeft + scrollPosition().x,
				y: e.clientY - canvas.offsetTop + scrollPosition().y
			}
		}
		function on_mousedown(e){
		sauvegarde.push(ctx.getImageData(0,0,width,height))
			ink = true;
			lastPos = calculateMouseCoord(e);
		}
		function on_mousemove(e){
			if(ink == false){return false} 
			else{
				mousePos = calculateMouseCoord(e);
				distance.x = lastPos.x - mousePos.x;
				distance.y = lastPos.y - mousePos.y;
				drawOnMyCanvas(e);
			}
		}
		function on_mouseup(e){
			ink = false;
		}
		function drawOnMyCanvas(e){
			ctx.strokeStyle = colorLine;
			ctx.lineCap = 'round';
			ctx.lineJoin = 'round';
			ctx.lineWidth = lineWidth;
			ctx.fillStyle = colorFill;
			
			if(outil === "pinceau" && isStroke === "checked"){
				ctx.beginPath();
				ctx.moveTo(lastPos.x, lastPos.y);
				ctx.lineTo(mousePos.x, mousePos.y);
				ctx.stroke();
				ctx.closePath();
				lastPos.x = mousePos.x;
				lastPos.y = mousePos.y;
			}
			if(outil === "chenille"){
				var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
				if(isFill === "checked"){
					ctx.beginPath();
					ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
					ctx.fill();
					ctx.closePath();
				}
				if(isStroke === "checked"){
					ctx.beginPath();
					ctx.arc(mousePos.x, mousePos.y, Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y)+(lineWidth/2)), Math.PI*2, false);
					ctx.stroke();
					ctx.closePath();
				}
				lastPos.x = mousePos.x;
				lastPos.y = mousePos.y;
			}
			if(outil === "cercle"){
				var i = Number(sauvegarde.length) -1;
				ctx.putImageData(sauvegarde[i], 0, 0)
				if(isFill === "checked"){
					ctx.beginPath();
					ctx.arc(lastPos.x,lastPos.y, Math.abs(distance.x), Math.PI*2, false);
					ctx.fill();
					ctx.closePath();
				}
				if(isStroke === "checked"){
					ctx.beginPath();
					ctx.arc(lastPos.x,lastPos.y, (Math.abs(distance.x)+(lineWidth/2)), Math.PI*2, false);
					ctx.stroke();
					ctx.closePath();
				}
			}
			if(outil === "rectangle"){
				var i = Number(sauvegarde.length) -1;
				ctx.putImageData(sauvegarde[i], 0, 0);
				if(touchPressed === 16){distance.y = distance.x};
				if(isFill === "checked"){
					ctx.beginPath();
					ctx.rect(lastPos.x,lastPos.y, distance.x*-1, distance.y*-1);
					ctx.fill();
					ctx.closePath();
				}

				//bad
/*
				if(isStroke === "checked"){
					ctx.beginPath();
					ctx.rect(lastPos.x-(lineWidth/2),lastPos.y-(lineWidth/2), (distance.x-lineWidth)*-1, (distance.y-lineWidth)*-1);
					ctx.stroke();
					ctx.closePath();
				}
*/
				//bad - end

				//good
				
				if(isStroke === "checked"){
					var decalage = {x:lineWidth, y:lineWidth}
					if(distance.x > 0){
						decalage.x = -decalage.x;
					}
					if(distance.y > 0){
						decalage.y = -decalage.y;
					}
					ctx.beginPath();
					ctx.rect(lastPos.x-(decalage.x/2),lastPos.y-(decalage.y/2), distance.x*-1 + decalage.x, distance.y *-1 + decalage.y);
					ctx.stroke();
					ctx.closePath();
				}
				
				//good - end
			}
		}
	}