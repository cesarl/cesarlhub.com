window.addEventListener('load', canvasApplication, false); 
function canvasApplication(){
    var width = 840;
    var height = 561;
    var canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    var lastPos = {x:0, y:0};
    var mousePos = {x:0, y:0};
    var mouseDiff = {x:0, y:0};
    var ink = false;
    var colorLine = "#BABA12"
    var lineWidth = 10
    var myImage = new Image();
    myImage.src = "/assets/js/experiences/drawing_board/images/ash2.jpg";
    myImage.addEventListener('load', imageIsLoaded, false);
    
    pageMargin = document.getElementsByTagName("body")[0];
    
    var sauvegarde = [];
    
    /* ----------------------- NEW ----------------------- */
    
    //on ajoute une variable outil qui permettra de switcher entre
    //deux outils diff�rents
    //le pinceau que l'on a d�ja vu dans les articles pr�c�dedents
    //et la "chenille", notre nouvelle brosse
    var outil = ["pinceau","chenille"];
    
    //l'outil par d�faut sera le pinceau (=0)
    var outilId = 0;
    /* ----------------------- ^^^ ----------------------- */
    
    function imageIsLoaded(){
	ctx.drawImage(myImage, 0,0);
	canvas.addEventListener("mousedown", on_mousedown, false);
	canvas.addEventListener("mousemove", on_mousemove, false);
	canvas.addEventListener("mouseup", on_mouseup, false);
	window.addEventListener("keyup",oneKeyUp,true);
    }
    function oneKeyUp(e){
	var letterPressed = String.fromCharCode(e.keyCode);
	if(letterPressed === "Z"){
	    var i = Number(sauvegarde.length);
	    if(i>0){
		ctx.putImageData(sauvegarde.pop(), 0, 0);
	    }
	}
	
	if(letterPressed === "S"){
	    var canvasToImg = canvas.toDataURL();
	    window.open(canvasToImg,"nom de la fen�tre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
	}
	/* --------------------- NEW --------------------- */
	//si la touche appuy�e est �quale � "T"
	if(letterPressed === "T"){
	    //nous rajoutons 1 � outilId
	    outilId++
	    //si outilId viens � d�passer le nombre d'outils disponibles,
	    //alors, outilId = 0
	    if(outilId == outil.length){
		outilId = 0;
	    }
	}
	/* --------------------- ^^^ --------------------- */
    }
    
    function scrollPosition(e) {
	return {
	    x: document.scrollLeft || window.pageXOffset,
	    y: document.scrollTop || window.pageYOffset
	};
    }
    function calculateMouseCoord(e){
	return{
	    x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
	    y: e.clientY - canvas.offsetTop - pageMargin.offsetTop + scrollPosition().y
	}
    }
    function on_mousedown(e){
	sauvegarde.push(ctx.getImageData(0,0,width,height))
	ink = true;
	lastPos = calculateMouseCoord(e);
    }
    function on_mousemove(e){
	if(ink == false){return false} 
	else{
	    mousePos = calculateMouseCoord(e);
	    drawOnMyCanvas(e);
	}
    }
    function on_mouseup(e){
	ink = false;
    }
    function drawOnMyCanvas(e){
	/* --------------------- NEW --------------------- */
	//si outil = pinceau
	if(outil[outilId] === "pinceau"){
	    //alors on applique ce que l'on a d�j� vu dans les articles pr�c�dents
	    ctx.beginPath();
	    ctx.strokeStyle = colorLine;
	    ctx.lineWidth = lineWidth;
	    ctx.lineCap = 'round';
	    ctx.lineJoin = 'round';
	    ctx.moveTo(lastPos.x, lastPos.y);
	    ctx.lineTo(mousePos.x, mousePos.y);
	    ctx.stroke();
	    ctx.closePath();
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
	//si outil = chenille,
	//alors on active notre nouvele brosse
	if(outil[outilId] === "chenille"){
	    //nous commen�ons par servir de la diff�rence entre la position pr�c�dente de la souris,
	    //et celle actuelle. Nous l'appelerons "rayon", cela nous permettra
	    //d'adapter le rayon en fonction de la vitesse de d�placement
	    //de la souris
	    //Math.abs nous permettra de convertir en positif le resultat si il est n�gatif (car la souris peut aller de droite � gauche ou de bas en haut)
	    var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
	    
	    //�a on connait d�j�
	    ctx.beginPath();
	    
	    //on d�finit le fillStyle (nous avons d�j� vu le strokeStyle, pour la couleur des contours)
	    //ici nous voulons des cercles plein, donc nous utilisons fillStyle --> http://s4r.fr/ne2Y2i
	    ctx.fillStyle = "#D0145A";
	    
	    //nous allons dessiner un cercle
	    //a chaque mousemove en nous servant de la distance parcourue par la souris pour
	    //en d�finir le diam�tre -- Pour en savoir plus sur arc() --> http://s4r.fr/qkryVb
	    ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
	    //ici nous appliquons le fillStyle, le cercle devient donc visible
	    ctx.fill();
	    //nous fermons le path
	    ctx.closePath();
	    
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
	/* --------------------- ^^^ --------------------- */
    }
}
