(function ($) {
	Drupal.behaviors.cesarlc = {};
	Drupal.behaviors.cesarlc.attach = function(context) {

		var width = 910;
		var height = 561;
		var canvas = document.getElementById("canvas");
		canvas.width = width;
		canvas.height = height;
		var ctx = canvas.getContext('2d');
		var lastPos = {x:0, y:0};
		var mousePos = {x:0, y:0};
		var mouseDiff = {x:0, y:0};
		var ink = false;
		var colorLine = "#BABA12"
		var lineWidth = 10
		var myImage = new Image();
		myImage.src ="/sites/www.cesar.lc/themes/cesarlc/experiences/drawing_board/images/ash2.jpg";
		myImage.addEventListener('load', imageIsLoaded, false);
		var letterPressed;
		var touchPressed;
		var sauvegarde = [];
		var outil = "pinceau";
		var action = "";
		var distance = {x:10, y:10};
		var cropArea = {x:width, y:height};
		var cropPoints = {x:0, y:0};
		var pageMargin = document.getElementById("page");
		$("#toolbar").find("."+outil).addClass("active");
		
		function imageIsLoaded(){
			ctx.drawImage(myImage, 0,0);

			canvas.addEventListener("mousedown", on_mousedown, false);
			canvas.addEventListener("mousemove", on_mousemove, false);
			canvas.addEventListener("mouseup", on_mouseup, false);
			window.addEventListener("keyup",oneKeyUp,true);
			window.addEventListener("keydown",oneKeyDown,true);

		$("#toolbar .bouton").click(function(){
			if($(this).hasClass("tool")){
				if(outil === "crop" && sauvegarde.length > 0 ){
					ctx.putImageData(sauvegarde.pop(), 0, 0);
					cropArea = {x:width, y:height};
					cropPoints = {x:0, y:0};
				}
				if($(this).hasClass("crop")){
					action = "crop";
				}
				$(this).addClass("active")
				$(".tool").not(this).removeClass("active");
				outil = $(this).attr("toolstype");
			};
			if($(this).hasClass("action")){
				action = $(this).attr("toolstype");
				actions();
			}
		});
		}
		function oneKeyUp(e){
			letterPressed = String.fromCharCode(e.keyCode);
			touchPressed = "";
			actions(e);
		}
		function oneKeyDown(e){
			touchPressed = e.keyCode;
		}
		function actions(){
			if(action === "historique" || letterPressed === "Z"){
				var i = Number(sauvegarde.length);
				if(i>0){
					ctx.putImageData(sauvegarde.pop(), 0, 0);
				}
				action ="nothing";
			}
		
			if(action === "sauvegarde" || letterPressed === "S"){
				var canvasToImg = canvas.toDataURL();
				window.open(canvasToImg,"nom de la fen�tre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
				action ="nothing";
			}
			if(action === "crop"){
				sauvegarde.push(ctx.getImageData(0,0,width,height));
				action = "nothing";
				var canvasCropSelection = ctx.getImageData(cropPoints.x,cropPoints.y,cropArea.x,cropArea.y);
				ctx.clearRect(0,0,width,height);
				ctx.putImageData(canvasCropSelection,0,0);
				var canvasToImg = canvas.toDataURL();
				window.open(canvasToImg,"nom de la fen�tre","left=0,top=0 ,width=" + width + ",height=" + height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
			}
		}

		function scrollPosition(e) {
			return {
				x: document.scrollLeft || window.pageXOffset,
				y: document.scrollTop || window.pageYOffset
			};
		}
		function calculateMouseCoord(e){
			return{
				x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
				y: e.clientY - canvas.offsetTop - pageMargin.offsetTop + scrollPosition().y
			}
		}
		function on_mousedown(e){
		sauvegarde.push(ctx.getImageData(0,0,width,height))
			ink = true;
			lastPos = calculateMouseCoord(e);
		}
		function on_mousemove(e){
			if(ink == false){return false} 
			else{
				mousePos = calculateMouseCoord(e);
				distance.x = lastPos.x - mousePos.x;
				distance.y = lastPos.y - mousePos.y;
				drawOnMyCanvas(e);
			}
		}
		function on_mouseup(e){
			ink = false;
			if(outil === "crop" && action === "crop"){
				actions();
				outil = "pinceau";
				$(".pinceau").addClass("active")
				$(".crop").removeClass("active");
			}
		}
		function drawOnMyCanvas(e){
			if(outil === "pinceau"){
				ctx.beginPath();
				ctx.strokeStyle = colorLine;
				ctx.lineWidth = lineWidth;
				ctx.lineCap = 'round';
				ctx.lineJoin = 'round';
				ctx.moveTo(lastPos.x, lastPos.y);
				ctx.lineTo(mousePos.x, mousePos.y);
				ctx.stroke();
				ctx.closePath();
				lastPos.x = mousePos.x;
				lastPos.y = mousePos.y;
			}
			if(outil === "chenille"){
				var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
				ctx.beginPath();
				ctx.fillStyle = "#D0145A";
				ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
				ctx.fill();
				ctx.closePath();
				lastPos.x = mousePos.x;
				lastPos.y = mousePos.y;
			}
			if(outil === "cercle"){
				var i = Number(sauvegarde.length) -1;
				ctx.putImageData(sauvegarde[i], 0, 0)
				ctx.strokeStyle = colorLine;
				ctx.strokeWidth = lineWidth;
				ctx.fillStyle = "#D0145A";
				ctx.beginPath();
				ctx.moveTo(lastPos.x, lastPos.y);
				ctx.arc(lastPos.x,lastPos.y, Math.abs(distance.x), Math.PI*2, false);
				ctx.fill();
				ctx.closePath();
			}
			if(outil === "rectangle"){
				var i = Number(sauvegarde.length) -1;
				ctx.putImageData(sauvegarde[i], 0, 0)
				ctx.strokeStyle = colorLine;
				ctx.strokeWidth = lineWidth;
				ctx.fillStyle = "#D0145A";
				ctx.beginPath();
				ctx.moveTo(lastPos.x, lastPos.y);
				if(touchPressed === 16){distance.y = distance.x};
				ctx.rect(lastPos.x,lastPos.y, distance.x*-1, distance.y*-1, Math.PI*2, false);
				ctx.fill();
				ctx.closePath();
			}
			if(outil === "crop"){
				var i = Number(sauvegarde.length) -1;
				ctx.putImageData(sauvegarde[i], 0, 0)
				ctx.strokeStyle = colorLine;
				ctx.strokeWidth = 1;
				ctx.strokeStyle = "#D0145A";
				ctx.beginPath();
				ctx.moveTo(lastPos.x, lastPos.y);
				if(touchPressed === 16){distance.y = distance.x};
				ctx.rect(lastPos.x,lastPos.y, distance.x*-1, distance.y*-1, Math.PI*2, false);
				ctx.stroke();
				ctx.closePath();
				cropPoints = {x:lastPos.x +1, y:lastPos.y +1};
				cropArea = {x:distance.x*-1 -2, y:distance.y*-1 -2};
			}
		}




	}
})(jQuery);