window.addEventListener('load', canvasApplication, false);


function canvasApplication(){

    var noSelect = document.getElementById("application");
    document.onselectstart = function (){ return false; };

    var width = 640;
    var height = 480;

    var positionLayer = document.getElementById("position-tracker");
    positionLayer.width=width;
    positionLayer.height=height;

    //picker
    var pickerLayer = document.getElementById("picker-layer");
    var pkr = pickerLayer.getContext("2d");
    var pkrSize = 120;
    pickerLayer.width = pkrSize;
    pickerLayer.height = pkrSize;
    var hexadecimalPkr;

    //general
    var canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    var lastPos = {x:0, y:0};
    var mousePos = {x:0, y:0};
    var ink = false;
    var myImage = new Image();
    myImage.src = "/assets/js/experiences/drawing_board/images/cateye.jpeg";
    myImage.addEventListener('load', imageIsLoaded, false);

    function imageIsLoaded(){
	ctx.drawImage(myImage, 0,0);

	positionLayer.addEventListener("mousedown", on_mousedown, false);
	positionLayer.addEventListener("mousemove", on_mousemove, false);
	positionLayer.addEventListener("mouseup", on_mouseup, false);

    }

    function scrollPosition(e) {
	return {
	    x: document.scrollLeft || window.pageXOffset,
	    y: document.scrollTop || window.pageYOffset
	};
    }
    function calculateMouseCoord(e){
	return{
	    x: e.clientX - positionLayer.offsetLeft + scrollPosition().x,
	    y: e.clientY - positionLayer.offsetTop + scrollPosition().y
	}
    }
    function on_mousedown(e){
	ink = true;
	lastPos = calculateMouseCoord(e);
    }
    function on_mousemove(e){
	if(ink == false){return false}
	else{
	    mousePos = calculateMouseCoord(e);
	    drawOnMyCanvas(e);
	}
    }
    function on_mouseup(e){
	ink = false;
    }
    function drawOnMyCanvas(e){
	pkr.clearRect(0,0,120,120);
	pkr.arc(60,60, 57, Math.PI*2, false);
	pkr.save();
	pkr.clip();

	var zoomX = mousePos.x-10;
	var zoomY = mousePos.y-10;
	var zoomW = 20;
	var zoomH = 20;

	if(mousePos.x < 10){
	    zoomX = mousePos.x;
	}
	if(mousePos.y < 10){
	    zoomY = mousePos.y;
	}
	if(mousePos.x >= width-10){
	    zoomX = width-zoomW;
	}
	if(mousePos.y >= height-10){
	    zoomY = height-zoomH;
	}

	pkr.drawImage(canvas, zoomX, zoomY,zoomW,zoomH,0,0,120,120);
/*
  var pixelZoom = pkr.getImageData(0,0,120,120);
  var red = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 0];
  var green = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 1];
  var blue = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 2];
  var pickerColor = "rgb("+red+", "+green+", "+blue+")";
*/
	var pixelZoom = pkr.getImageData(60,60,1,1);
	var pickerColor = pixelZoom.data;
	hexadecimalPkr = "#" + ("000000" + rgbToHex(pickerColor[0], pickerColor[1], pickerColor[2])).slice(-6);

	pkr.restore();
	pkr.strokeStyle = "#000000";
	pkr.beginPath();
	pkr.arc(60,60, 45, Math.PI*2, false);
	pkr.stroke();
	pkr.closePath();
	pkr.strokeStyle = hexadecimalPkr;
	pkr.lineCap = 'round';
	pkr.lineJoin = 'round';
	pkr.lineWidth = 10;
	pkr.beginPath();
	pkr.arc(60,60, 55, Math.PI*2, false);
	pkr.stroke();
	pkr.closePath();
	$("#picker-layer").css({marginLeft:mousePos.x-60,marginTop:mousePos.y-60})
	$(".colorPickerValue").empty().html(hexadecimalPkr).css({border:hexadecimalPkr+" 3px solid"});
    }
}
// convertir une valeur rgb en hexadecimal. Pour plus d'info ici --> http://s4r.fr/nWSJfQ
function rgbToHex(r, g, b) {
    if (r > 255 || g > 255 || b > 255)
	throw "Invalid color component";
    return ((r << 16) | (g << 8) | b).toString(16);
}