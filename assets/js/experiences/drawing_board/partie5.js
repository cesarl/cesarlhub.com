$(document).ready(function(){
    var myImage = new Image();
    myImage.src = "/assets/js/experiences/drawing_board/images/ash2.jpg";
    myImage.addEventListener('load', imageIsLoaded, false);
    var width = 840;
    var height = 561;
    var canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    var lastPos = {x:0, y:0};
    var mousePos = {x:0, y:0};
    var mouseDiff = {x:0, y:0};
    var ink = false;
    var colorLine = "#BABA12"
    var lineWidth = 10
    var letterPressed;
    var sauvegarde = [];
    var outil = "pinceau";
    var action = "";
    var circleRadius = 10;
    var lastCircleRadius;
    $("#toolbar").find("."+outil).addClass("active");
    var pageMargin = document.getElementsByTagName("body")[0];

    function imageIsLoaded(){
	ctx.drawImage(myImage, 0,0);

	canvas.addEventListener("mousedown", on_mousedown, false);
	canvas.addEventListener("mousemove", on_mousemove, false);
	canvas.addEventListener("mouseup", on_mouseup, false);
	window.addEventListener("keyup",oneKeyUp,true);

	$("#toolbar .bouton").click(function(){
	    if($(this).hasClass("tool")){
				$(this).addClass("active")
		$(".tool").not(this).removeClass("active");
		outil = $(this).attr("toolstype");
	    };
	    if($(this).hasClass("action")){
		action = $(this).attr("toolstype");
		actions();
	    }
	});
    }
    function oneKeyUp(e){
	letterPressed = String.fromCharCode(e.keyCode);
	actions(e);
    }
		function actions(){
		    if(action === "historique" || letterPressed === "Z"){
			var i = Number(sauvegarde.length);
			if(i>0){
			    ctx.putImageData(sauvegarde.pop(), 0, 0);
			}
		    }
		    if(action === "sauvegarde" || letterPressed === "S"){
			var canvasToImg = canvas.toDataURL();
			window.open(canvasToImg,"nom de la fenêtre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
		    }
		    action ="";
		}

    function scrollPosition(e) {
	return {
	    x: document.scrollLeft || window.pageXOffset,
	    y: document.scrollTop || window.pageYOffset
	};
    }
    function calculateMouseCoord(e){
	return{
	    x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
	    y: e.clientY - canvas.offsetTop - pageMargin.offsetTop + scrollPosition().y
	}
    }
    function on_mousedown(e){
	sauvegarde.push(ctx.getImageData(0,0,width,height))
	ink = true;
	lastPos = calculateMouseCoord(e);
    }
    function on_mousemove(e){
	if(ink == false){return false} 
	else{
	    mousePos = calculateMouseCoord(e);
	    circleRadius = Math.abs(lastPos.x - mousePos.x);
	    drawOnMyCanvas(e);
	}
    }
    function on_mouseup(e){
	ink = false;
    }
    function drawOnMyCanvas(e){
	if(outil === "pinceau"){
	    ctx.beginPath();
	    ctx.strokeStyle = colorLine;
	    ctx.lineWidth = lineWidth;
	    ctx.lineCap = 'round';
	    ctx.lineJoin = 'round';
	    ctx.moveTo(lastPos.x, lastPos.y);
	    ctx.lineTo(mousePos.x, mousePos.y);
	    ctx.stroke();
	    ctx.closePath();
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
	if(outil === "chenille"){
	    var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
	    ctx.beginPath();
	    ctx.fillStyle = "#D0145A";
	    ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
	    ctx.fill();
	    ctx.closePath();
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
	if(outil === "cercle"){
	    var i = Number(sauvegarde.length) -1;
	    ctx.putImageData(sauvegarde[i], 0, 0)
	    ctx.strokeStyle = colorLine;
	    ctx.strokeWidth = lineWidth;
	    ctx.fillStyle = "#D0145A";
	    ctx.beginPath();
	    ctx.moveTo(lastPos.x, lastPos.y);
	    ctx.arc(lastPos.x,lastPos.y, circleRadius, Math.PI*2, false);
	    ctx.fill();
	    ctx.closePath();
	}
    }
});

