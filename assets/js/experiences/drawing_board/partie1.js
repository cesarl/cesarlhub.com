$(document).ready(function(){
    window.addEventListener('load', canvasApplication, false);

    function canvasApplication(){
	var width = 840;
	height = 561;
	canvas = document.getElementById("canvas");
	canvas.width = width;
	canvas.height = height;
	ctx = canvas.getContext('2d');
	lastPos = {x:0, y:0};
	mousePos = {x:0, y:0};
	ink = false;
	colorLine = "#BABA12";
	lineWidth = 10;
	pageMargin = document.getElementsByTagName("body")[0];
	myImage = new Image();
	myImage.src = "/assets/js/experiences/drawing_board/images/ash1.jpg";
	myImage.addEventListener('load', imageIsLoaded, false);

	function imageIsLoaded(){
		ctx.drawImage(myImage, 0,0);
		canvas.addEventListener("mousedown", on_mousedown, false);
		canvas.addEventListener("mousemove", on_mousemove, false);
		canvas.addEventListener("mouseup", on_mouseup, false);
	}
	function scrollPosition(e) {
		return {
			x: document.scrollLeft || window.pageXOffset,
			y: document.scrollTop || window.pageYOffset
		};
	}
	function calculateMouseCoord(e){
		return{
			x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
			y: e.clientY - canvas.offsetTop - pageMargin.offsetTop  + scrollPosition().y
		}
	}
	function on_mousedown(e){
		ink = true;
		lastPos = calculateMouseCoord(e);
	}
	function on_mousemove(e){
		if(ink == false){return false} 
		else{
			mousePos.x = calculateMouseCoord(e).x;
			mousePos.y = calculateMouseCoord(e).y;
			drawOnMyCanvas(e);
		}
	}
	function on_mouseup(e){
		ink = false;
	}
	function drawOnMyCanvas(e){
		ctx.beginPath();
		ctx.strokeStyle = colorLine;
		ctx.lineWidth = lineWidth;
		ctx.lineCap = 'round';
		ctx.lineJoin = 'round';
		ctx.moveTo(lastPos.x, lastPos.y);
		ctx.lineTo(mousePos.x, mousePos.y);
		ctx.stroke();
		ctx.closePath();
		lastPos.x = mousePos.x;
		lastPos.y = mousePos.y;
	}
    }
});
