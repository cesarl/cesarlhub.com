$(document).ready(function(){
    window.addEventListener('load', canvasApplication, false); 
    function canvasApplication(){
	var width = 840;
	var height = 561;
	var canvas = document.getElementById("canvas");
	canvas.width = width;
	canvas.height = height;
	var ctx = canvas.getContext('2d');
	var lastPos = {x:0, y:0};
	var mousePos = {x:0, y:0};
	var mouseDiff = {x:0, y:0};
	var ink = false;
	var colorLine = "#BABA12"
	var lineWidth = 10
	var myImage = new Image();
	myImage.src = "/assets/js/experiences/drawing_board/images/ash2.jpg";
	myImage.addEventListener('load', imageIsLoaded, false);
	pageMargin = document.getElementsByTagName("body")[0];
	var sauvegarde = [];
	
	function imageIsLoaded(){
	    ctx.drawImage(myImage, 0,0);
	    canvas.addEventListener("mousedown", on_mousedown, false);
	    canvas.addEventListener("mousemove", on_mousemove, false);
	    canvas.addEventListener("mouseup", on_mouseup, false);
	    /* --------------------- NEW --------------------- */
	    //nous modifions notre ancien eventlistener qui lan�ait la foncion ctrlz
	    //window.addEventListener("keyup",ctrlz,true)
	    //pour la remplacer par une plus g�n�rale
	    window.addEventListener("keyup",oneKeyUp,true);
	    /* --------------------- ^^^ --------------------- */
	}
	/* --------------------- NEW --------------------- */
	function oneKeyUp(e){
	    //letterPressed = la lettre correspondante � la touche appuy�e
	    var letterPressed = String.fromCharCode(e.keyCode);
	    if(letterPressed === "Z"){
		var i = Number(sauvegarde.length);
		if(i>0){
		    ctx.putImageData(sauvegarde.pop(), 0, 0);
		}
	    }
	    
	    //si la touche appuy�e est �gale � "S"
	    if(letterPressed === "S"){
		//nous convertissons le canvas en data Url
		// in English : returns a data URL containing a base64 encoding of the raw image bytes - yeah !!!
		var canvasToImg = canvas.toDataURL();
		//on ouvre une nouvelle fenetre avec notre beau dessin
		//que l'on peu enregitrer-sous
		//coooooooooooooooool
		window.open(canvasToImg,"nom de la fen�tre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
	    }
	}
	/* --------------------- ^^^ --------------------- */
	
	function scrollPosition(e) {
	    return {
		x: document.scrollLeft || window.pageXOffset,
		y: document.scrollTop || window.pageYOffset
	    };
	}
	function calculateMouseCoord(e){
	    return{
		x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
		y: e.clientY - canvas.offsetTop - pageMargin.offsetTop + scrollPosition().y
	    }
	}
	function on_mousedown(e){
	    sauvegarde.push(ctx.getImageData(0,0,width,height))
	    ink = true;
	    lastPos = calculateMouseCoord(e);
	}
	function on_mousemove(e){
	    if(ink == false){return false} 
	    else{
		mousePos = calculateMouseCoord(e);
		drawOnMyCanvas(e);
	    }
	}
	function on_mouseup(e){
	    ink = false;
	}
	function drawOnMyCanvas(e){
	    ctx.beginPath();
	    ctx.strokeStyle = colorLine;
	    ctx.lineWidth = lineWidth;
	    ctx.lineCap = 'round';
	    ctx.lineJoin = 'round';
	    ctx.moveTo(lastPos.x, lastPos.y);
	    ctx.lineTo(mousePos.x, mousePos.y);
	    ctx.stroke();
	    ctx.closePath();
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
    }
});
