var   canvas = document.getElementById("canvas")
, ctx = canvas.getContext("2d")
, ink = true
, w = 567
, h = 377
, clickPos = {
    x: 0
    , y: 0
}
, shape = []
, shapes = []
, bg = new Image()
, pageMargin = document.getElementById("container");

bg.src = "/assets/js/experiences/drawing_board/images/chouette.jpeg";

canvas.width = w;
canvas.height = h;

bg.addEventListener("load", init, false);


function init() {
    
    canvas.addEventListener("mousedown", on_mousedown, false);
    canvas.addEventListener("mouseup", on_mouseup, false);
    
    function drawBackground() {
	ctx.drawImage(bg,0,0)
    }
    
    function on_mousedown(e) {
	if(ink == false){return false}
	
	clickPos = calculateMouseCoord(e);
		
	if(shape.length == 0) {
	    shape.push(clickPos);
	    ink = false;
	    drawOnCanvas(true);
	    
	} else if( Math.abs(clickPos.x - shape[0].x ) <= 5
		   && Math.abs(clickPos.y - shape[0].y ) <= 5) {
	    ink = false;
	    drawOnCanvas(false);
	} else {
	    shape.push(clickPos);
	    ink = false;
	    drawOnCanvas(true);
	}
    }
    function on_mouseup(e) {
	ink = true;
    }
    
	function drawOnCanvas(inDraw) {
	    drawBackground();
	    ctx.fillStyle = "rgba(255,255,255,0.4)";
	    ctx.strokeStyle = "#caca23";
	    if(shapes.length > 0) {
		for(var j = 0, mj = shapes.length; j < mj; j++) {
		    drawPoly(shapes[j], false);
		}
	    }
		if(!inDraw) {
		    shapes.push(shape);
		    drawPoly(shape, inDraw)
		    shape = [];
		} else {
		    drawPoly(shape, inDraw)
		}
	}
    
    function drawPoly(poly, inDraw){
	for(var i = 0, mi = poly.length; i < mi; i++) {
	    if(i == 0 && inDraw) {
		ctx.beginPath()
		ctx.arc(poly[i].x, poly[i].y, 10, 0, 360 / 180 * Math.PI, false);
		ctx.fill();
		ctx.beginPath();
		ctx.moveTo(poly[i].x, poly[i].y);
	    } else if (i == 0 && !inDraw) {
		ctx.beginPath();
		ctx.moveTo(poly[i].x, poly[i].y);
	    } else {
		ctx.lineTo(poly[i].x, poly[i].y)
	    }
	    if(i == mi - 1 && inDraw) {
		ctx.stroke();
	    } else if(i == mi - 1 && !inDraw) {
		ctx.closePath();
		ctx.fill();
	    }
	}
    }
    drawBackground();
}

function scrollPosition(e) {
    return {
	x: document.scrollLeft || window.pageXOffset,
	y: document.scrollTop || window.pageYOffset
    };
}
function calculateMouseCoord(e) {
    return{
	x: e.clientX - canvas.offsetLeft + scrollPosition().x,
	y: e.clientY - canvas.offsetTop + scrollPosition().y
    }
}
