$(document).ready(function(){
    window.addEventListener('load', canvasApplication, false); 
    function canvasApplication(){
	var width = 840;
	var height = 561;
	var canvas = document.getElementById("canvas");
	canvas.width = width;
	canvas.height = height;
	var ctx = canvas.getContext('2d');
	var lastPos = {x:0, y:0};
	var mousePos = {x:0, y:0};
	var mouseDiff = {x:0, y:0};
	var ink = false;
	var colorLine = "#BABA12"
	var lineWidth = 10
	var myImage = new Image();
	myImage.src = "/assets/js/experiences/drawing_board/images/ash1.jpg";
	myImage.addEventListener('load', imageIsLoaded, false);
	pageMargin = document.getElementsByTagName("body")[0];
	/* --------------------- NEW --------------------- */
	//l'array "savegarde" stockera chaque �tape du dessin
	var sauvegarde = [];
	/* --------------------- ^^^ --------------------- */
	
	function imageIsLoaded(){
	    ctx.drawImage(myImage, 0,0);
	    canvas.addEventListener("mousedown", on_mousedown, false);
	    canvas.addEventListener("mousemove", on_mousemove, false);
	    canvas.addEventListener("mouseup", on_mouseup, false);
	    /* --------------------- NEW --------------------- */
	    //lorsque qu'une touche est appuy�e, on lance la fonction "ctrlz"
			window.addEventListener("keyup",ctrlz,true)
	}
	function ctrlz(e){
	    //letterPressed = la lettre correspondante � la touche appuy�e
	    var letterPressed = String.fromCharCode(e.keyCode);
	    //nous verifions que la touche appuy�e est �gual � "Z"
	    if(letterPressed === "Z"){
		//nous definissons "i" comme la longueur de l'array sauvagarde
		var i = Number(sauvegarde.length);
				//si le nombre de sauvgarde est sup�rieur � 0
		if(i>0){
		    //alors, nous appliquons au canvas la derni�re sauvegarde et nous la supprimons de l'array
		    ctx.putImageData(sauvegarde.pop(), 0, 0);
		}
	    }
	}
		/* --------------------- ^^^ --------------------- */
	function scrollPosition(e) {
	    return {
		x: document.scrollLeft || window.pageXOffset,
		y: document.scrollTop || window.pageYOffset
	    };
		}
	function calculateMouseCoord(e){
			return{
			    x: e.clientX - canvas.offsetLeft - pageMargin.offsetLeft + scrollPosition().x,
			    y: e.clientY - canvas.offsetTop - pageMargin.offsetTop + scrollPosition().y
			}
	}
	function on_mousedown(e){
	    /* --------------------- NEW --------------------- */
	    //� chaque click, avant de dessiner quoi que ce soit sur le canvas on effectue
	    //une savegarde du canvas
	    //ici, on sauvegarde la totalit� du canvas dans toute sa largeur et sa hauteur et on l'ajoute � l'array sauvegarde - http://s4r.fr/q6QvAU
		sauvegarde.push(ctx.getImageData(0,0,width,height))
	    /* --------------------- ^^^ --------------------- */
	    ink = true;
	    lastPos = calculateMouseCoord(e);
		}
	function on_mousemove(e){
	    if(ink == false){return false} 
	    else{
		mousePos = calculateMouseCoord(e);
		drawOnMyCanvas(e);
			}
	}
	function on_mouseup(e){
	    ink = false;
	}
	function drawOnMyCanvas(e){
	    ctx.beginPath();
	    ctx.strokeStyle = colorLine;
	    ctx.lineWidth = lineWidth;
	    ctx.lineCap = 'round';
	    ctx.lineJoin = 'round';
	    ctx.moveTo(lastPos.x, lastPos.y);
	    ctx.lineTo(mousePos.x, mousePos.y);
	    ctx.stroke();
	    ctx.closePath();
	    lastPos.x = mousePos.x;
	    lastPos.y = mousePos.y;
	}
    }
});
