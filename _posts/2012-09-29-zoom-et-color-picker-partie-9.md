---
layout: post
title: "Zoom et color picker (partie 9)"
description: ""
category: post
date: 2011-08-01 12:47:00
permalink: zoom-et-color-picker-partie-9
tags: []
attachment: "Drawing Board HTML5 - Démo étape 9 - Zoom et color picker"
illustration: "/images/chouette.jpg"
---
{% include JB/setup %}

Whou ! 9ème post ! Je m'impressionne,
Celui-là il va être marrant en plus (enfin il m'a fait rigoler moi). On va ajouter une pipette qui ira chercher directement sur le canvas les couleurs. Au passage on rencontrera quelques nouveautés.
Étant donné que je rajoute les fonctionnalités sur le code de l'ancien post, sans savoir quel sera le prochain, le code de notre drawing board commence à vraiment être un peu long et désordonné. Donc pour mieux illustrer la nouvelle fonctionnalité, nous allons ici reprendre du début et n'ajouter que notre outil pipette.

On commence par poser les bases que l'on trouve dans tous les posts précédents. En y ajoutant quelques nouveautés.

{% highlight javascript %}
//dimensions de la surface de travail
var width = 640;
var height = 480;

//nous ne trackons plus la position de la souris en fonction du canvas de dessin mais en fonction du div #position-tracker
var positionLayer = document.getElementById("position-tracker");
positionLayer.width=width;
positionLayer.height=height;

//notre canvas sur lequel nous dessinons, avec sa belle image de fond
var canvas = document.getElementById("canvas");
canvas.width = width;
canvas.height = height;
var ctx = canvas.getContext('2d');
var myImage = new Image();
myImage.src = "images/animal.jpeg";


// les variables qui stockerons les coordonnées de la souris et les distances parcourues
var lastPos = {x:0, y:0};
var mousePos = {x:0, y:0};

//block les fonctionnalités d'édition si false, les autorisent si true
var ink = false;

myImage.addEventListener('load', imageIsLoaded, false);
{% endhighlight %}

Nous ajoutons donc dans cet exemple le position tracker qui n'est rien d'autre qu'un div transparent en z-index 100 avec les mêmes dimensions et la même position que le canvas de dessins. Il nous permettra d'ajouter des calques canvas par-dessus notre canvas dessin et de continuer à suivre les clicks et mousemove.
Nous allons donc mapper les eventlistener (mousedown, mousemove et mouseup) uniquement sur cet élément.

{% highlight javascript %}
positionLayer.addEventListener("mousedown", on_mousedown, false);
positionLayer.addEventListener("mousemove", on_mousemove, false);
positionLayer.addEventListener("mouseup", on_mouseup, false);
{% endhighlight %}

Mais n'oublions pas avant d'ajouter les éléments indispensables à notre pipette :

- Le canvas sur lequel s'affichera le zoom et la couleur du pixel central.
- La variable qui stockera en hexadécimal la couleur du pixel central.

{% highlight javascript %}
var pickerLayer = document.getElementById("picker-layer");
var pkr = pickerLayer.getContext("2d");
var pkrSize = 120;
pickerLayer.width = pkrSize;
pickerLayer.height = pkrSize;
var hexadecimalPkr;
{% endhighlight %}

Là encore, rien de bien sorcier.
Toujours pareil à ce que l'on a pu voir avant, nous définissons les fonctions :

- `scrollPosition` pour calculer la distance en x et y du scroll sur la page.
- `calculateMouseCoord` qui retournera la position de la souris sur le tracker layer.
- on_mousedown
- on_mousemove
- on_mouseup

Venons en maintenant à la partie intéressante.
Nous voulons, que soit affiché à l'endroit de la souris un zoom de l'image de fond au sein d'un cercle de la couleur du pixel central. (cf démo).
Pour commencer, nous effaçons tout dans ce canvas (à chaque mousemove nous redessinons le zoom et le cercle de couleur).
`pkr.clearRect(0,0,pkrSize,pkrSize);`
Puis, nous traçons un premier cercle
`pkr.arc(60,60, 60, Math.PI*2, false);`
Et non, je n'ai pas oublié avant de préciser le fillStyle lineWidth, strokeStyle etc etc. Alors pourquoi ? Parce que je vais m'en servir pour quelque chose de nouveau ! Le clip !
Nous voulons que le zoom apparaisse dans un cercle, il ne faut donc pas que ce dernier en sorte, pour cela nous allons nous servir du cercle que nous venons de dessiner comme un masque. Et en canvas, on appelle ça clip.
Mais avant de cliper notre canvas, nous allons utiliser save() pour sauvegarder l'état de notre canvas. C'est bien mieux expliqué ici en ces termes :

_(...) cet état est un instantané de tous les styles et transformations qui ont été appliqués sur le canvas_

Ainsi, une fois le zoom clipé dans le cercle, nous restaurerons l'ancien état du canvas, c'est un dire un canvas sans clip. Et pour cela nous utiliserons restore(). Pour en savoir plus sur le clip et consulter quelques exemples, je vous conseille d'aller faire un tour [ici](http://robertnyman.com/2011/07/26/using-the-html5-canvas-clip-method-to-hide-certain-content/) et [ici](http://www.vectorlight.net/html5/tutorials/setting_a_clipping_region.aspx).

Nous pouvons maintenant définir les variables qui nous servirons à sélectionner la portion de l'image à zoomer

{% highlight javascript %}
var zoomX = mousePos.x-10;  // la position en x du début de la sélection. La position de la souris - 10px;
var zoomY = mousePos.y-10; // la position en y du début de la sélection. La position de la souris - 10px;
var zoomW = 20; // la largeur de la sélection
var zoomH = 20; // la hauteur de la sélection
{% endhighlight %}

Là, rien à dire, si ce n'est que plus la taille de la sélection à zoomer sera petite plus le zoom sera gros et pixelisera.

Ces variables telles que nous venons de les définir ne sont malheureusement pas suffisante. Pourquoi ? Quizz :
Si je mets ma souris dans le coin supérieur gauche (x:3px,y:3px) de l'image par exemple, que se passe-t-il ? ..... :D Réponse : 1 uoıʇdǝɔxǝ ɯop :ɹɹǝ‾ǝzıs‾xǝpuı :ɹoɹɹǝ ʇɥƃnɐɔun

![Bad !!!](/images/bad.jpg)

Et oui ! Ça tentera de sélectionner des pixels de notre image qui n'existent pas sur le canvas. Cf dessin.
Alors nous ici, parce qu'on est un peu flemmard on va faire simple pour corriger ça. On va tout simplement poser un limite au color picker. Par conséquent il nous sera impossible de sélectionner les couleurs des pixels à moins de 10px du bord de l'image. :(
Une solution consisterait à agrandir la surface du canvas mais pour cet exemple ça suffira entièrement.
Donc voila ce que nous allons faire :

{% highlight javascript %}
    /*si le cuseur est à moins de 10px du bord gauche*/
    if(mousePos.x < 10){
	zoomX = mousePos.x;
    }
    /*si le cuseur est à moins de 10px du bord supérieur*/
    if(mousePos.y < 10){
	zoomY = mousePos.y;
    }
    /*si le cuseur est à moins de 10px du bord droit*/
    if(mousePos.x >= width-10){
	zoomX = width-zoomW;
    }
    /*si le cuseur est à moins de 10px du bord inférieur*/
    if(mousePos.y >= height-10){
	zoomY = height-zoomH;
    }
{% endhighlight %}

Nous pouvons maintenant dessiner dans notre clipping mask la sélection :
`pkr.drawImage(canvas, zoomX, zoomY,zoomW,zoomH,0,0,120,120);`

Il est temps de calculer la couleur du pixel central, qui sera situé en x:60 et y:60 de notre zoom. Pour cela il existe plusieurs solutions. La première un peu plus longue nous aidera à mieux comprendre ce que nous faisons. Nous utiliserons finalement la deuxième bien plus simple.

Nous allons commencer par sélectionner l'array de pixel de notre zoom pour en extraire la valeur du pixel central.
`var pixelZoom = pkr.getImageData(0,0,120,120);`
Pour bien comprendre la suite je vous conseille de lire _[Pixel manipulation with canvas](https://developer.mozilla.org/en-US/docs/HTML/Canvas/Pixel_manipulation_with_canvas)_. Je ne pourrais pas mieux vous l'expliquer.
Chaque pixel est représenté par quatre valeurs : rouge, vert, bleu, transparence ; comprises entre 0 et 255, bref en rgba.
Nous allons donc pouvoir aller chercher la couleur en rgba du pixel de cette manière :

{% highlight javascript %}
    var red = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 0];
    var green = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 1];
    var blue = pixelZoom.data[((60*(pixelZoom.width*4)) + (60*4)) + 2];
    var pickerColor = "rgb("+red+", "+green+", "+blue+")";
{% endhighlight %}

Encore une fois c'est très bien expliqué [ici](http://stackoverflow.com/questions/6735470/get-pixel-color-from-canvas-on-mouseover), pas besoin que je me fatigue à vous l'expliquer plus mal en frenchi :)

Maintenant le seconde méthode ! La première nous aide à comprendre la structure des arrays de pixels que nous appelons avec la méthode getImageData
Nous n'allons prendre que le pixel qui nous intéresse : pixelZoom = pkr.getImageData(60,60,1,1); puis avec une belle fonction magique trouvée [ici](http://s4r.fr/nWSJfQ) nous allons en extraire la couleur au format hexadécimal.

{% highlight javascript %}
// convertir une valeur rgb en hexadecimal. Pour plus d'info ici --> http://s4r.fr/nWSJfQ
function rgbToHex(r, g, b) {
  if (r > 255 || g > 255 || b > 255)
    throw "Invalid color component";
  return ((r << 16) | (g << 8) | b).toString(16);																																		   }
var pixelZoom = pkr.getImageData(60,60,1,1);
var pickerColor = pixelZoom.data;
hexadecimalPkr = "#" + ("000000" + rgbToHex(pickerColor[0], pickerColor[1], pickerColor[2])).slice(-6);
{% endhighlight %}

C'est bon, nous avons fait le plus dur, maintenant, il ne nous reste plus qu'à peaufiner notre outil en traçant un cercle noir et un cercle de la couleur central autour de notre zoom.
Pour cela, nous commençons par restaurer l'état de notre canvas avec `restore()` pour sortir du clipping mask. Puis, nous pouvons nous mettre au dessin :

{% highlight javascript %}
pkr.restore();
pkr.strokeStyle = "#000000";
pkr.beginPath();
pkr.arc(60,60, 45, Math.PI*2, false);
pkr.stroke();
pkr.closePath();
pkr.strokeStyle = hexadecimalPkr;
pkr.lineCap = 'round';
pkr.lineJoin = 'round';
pkr.lineWidth = 10;
pkr.beginPath();
pkr.arc(60,60, 55, Math.PI*2, false);
pkr.stroke();
pkr.closePath();
{% endhighlight %}

Et voilà !

Comme je vous le disais c'est du test, c'est vite fait et je découvre en même temps que vous, donc n'hésitez pas à me faire part de vos idées pour améliorer tout ça.