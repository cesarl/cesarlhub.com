---
layout: post
title: "Ajouter une brosse custom a notre Canvas Drawing Board (partie 4)"
description: ""
category: post
date: 2011-07-26 16:56:00
permalink: /ajouter-un-brosse-custom-notre-canvas-drawing-board-partie-4
tags: []
illustration: "/images/tuttles.jpg"
attachment: "Drawing Board HTML5 - Démo étape 4 - brosse"
---
{% include JB/setup %}

On va juste s'amuser à ajouter une brosse différente à notre drawing board avant de passer aux formes géométriques. Tout a quasiment déjà été traité dans les parties précédentes de cette série d'article donc je ne vais pas m'éterniser à expliquer.

Pour changer d'outil nous allons nous servir de la touche "T".

Notre nouvel outil, va nous permettre d'introduire le cercle (arc), que nous retrouverons dans la prochaine partie. Nous allons donc créer une brosse qui va dessiner, non plus des lignes mais des cercles. Le diamètre de ces derniers variera en fonction de la vitesse de la souris ( de la distance qu'elle parcourt ). Plus la souris sera déplacée rapidement, plus le cercle sera large.

Je vous laisse lire le code, et jouer avec la démo ci-dessus (touche "T" pour changer d'outil)

{% highlight javascript %}
window.addEventListener('load', canvasApplication, false); 
function canvasApplication(){
    var width = 910;
    var height = 561;
    var canvas = document.getElementById("canvas");
    canvas.width = width;
    canvas.height = height;
    var ctx = canvas.getContext('2d');
    var lastPos = {x:0, y:0};
    var mousePos = {x:0, y:0};
    var mouseDiff = {x:0, y:0};
    var ink = false;
    var colorLine = "#BABA12"
    var lineWidth = 10
    var myImage = new Image();
    myImage.src = "images/ash2.jpg";
    myImage.addEventListener('load', imageIsLoaded, false);
     
    var sauvegarde = [];
     
/* ----------------------- NEW ----------------------- */
 
//on ajoute une variable outil qui permettra de switcher entre
//deux outils différents
//le pinceau que l'on a déja vu dans les articles précédents
//et la "chenille", notre nouvelle brosse
var outil = ["pinceau","chenille"];
 
//l'outil par défaut sera le pinceau (=0)
var outilId = 0;
/* ----------------------- ^^^ ----------------------- */
     
    function imageIsLoaded(){
        ctx.drawImage(myImage, 0,0);
        canvas.addEventListener("mousedown", on_mousedown, false);
        canvas.addEventListener("mousemove", on_mousemove, false);
        canvas.addEventListener("mouseup", on_mouseup, false);
        window.addEventListener("keyup",oneKeyUp,true);
    }
    function oneKeyUp(e){
        var letterPressed = String.fromCharCode(e.keyCode);
        if(letterPressed === "Z"){
            var i = Number(sauvegarde.length);
            if(i>0){
                ctx.putImageData(sauvegarde.pop(), 0, 0);
            }
        }
     
        if(letterPressed === "S"){
            var canvasToImg = canvas.toDataURL();
            window.open(canvasToImg,"nom de la fenêtre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
        }
        /* --------------------- NEW --------------------- */
        //si la touche appuyée est équale à "T"
        if(letterPressed === "T"){
            //nous rajoutons 1 à outilId
            outilId++
            //si outilId viens à dépasser le nombre d'outils disponibles,
            //alors, outilId = 0
            if(outilId == outil.length){
            outilId = 0;
            }
        }
        /* --------------------- ^^^ --------------------- */
    }
 
    function scrollPosition(e) {
        return {
            x: document.scrollLeft || window.pageXOffset,
            y: document.scrollTop || window.pageYOffset
        };
    }
    function calculateMouseCoord(e){
        return{
            x: e.clientX - canvas.offsetLeft + scrollPosition().x,
            y: e.clientY - canvas.offsetTop + scrollPosition().y
        }
    }
    function on_mousedown(e){
    sauvegarde.push(ctx.getImageData(0,0,width,height))
        ink = true;
        lastPos = calculateMouseCoord(e);
    }
    function on_mousemove(e){
        if(ink == false){return false} 
        else{
            mousePos = calculateMouseCoord(e);
            drawOnMyCanvas(e);
        }
    }
    function on_mouseup(e){
        ink = false;
    }
    function drawOnMyCanvas(e){
        /* --------------------- NEW --------------------- */
        //si outil = pinceau
        if(outil[outilId] === "pinceau"){
            //alors on applique ce que l'on a déjà vu dans les articles précédents
            ctx.beginPath();
            ctx.strokeStyle = colorLine;
            ctx.lineWidth = lineWidth;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            ctx.moveTo(lastPos.x, lastPos.y);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.stroke();
            ctx.closePath();
            lastPos.x = mousePos.x;
            lastPos.y = mousePos.y;
        }
        //si outil = chenille,
        //alors on active notre nouvele brosse
        if(outil[outilId] === "chenille"){
            //nous commençons par servir de la différence entre la position précédente de la souris,
            //et celle actuelle. Nous l'appellerons "rayon", cela nous permettra
            //d'adapter le rayon en fonction de la vitesse de déplacement
            //de la souris
            //Math.abs nous permettra de convertir en positif le resultat si il est négatif (car la souris peut aller de droite à gauche ou de bas en haut)
            var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
 
            //ça on connait déjà
            ctx.beginPath();
 
            //on définit le fillStyle (nous avons déjà vu le strokeStyle, pour la couleur des contours)
            //ici nous voulons des cercles plein, donc nous utilisons fillStyle --> http://s4r.fr/ne2Y2i
            ctx.fillStyle = "#D0145A";
     
            //nous allons dessiner un cercle
            //a chaque mousemove en nous servant de la distance parcourue par la souris pour
            //en définir le diamètre -- Pour en savoir plus sur arc() --> http://s4r.fr/qkryVb
            ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
            //ici nous appliquons le fillStyle, le cercle devient donc visible
            ctx.fill();
            //nous fermons le path
            ctx.closePath();
     
            lastPos.x = mousePos.x;
            lastPos.y = mousePos.y;
        }
    /* --------------------- ^^^ --------------------- */
    }
}
{% endhighlight %}