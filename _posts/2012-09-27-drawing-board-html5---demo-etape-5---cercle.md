---
layout: post
title: "Drawing Board HTML5 - Démo étape 5 - cercle"
description: ""
category: demo
tags: []
date: 2011-07-26 21:19:00
scripts: ["/assets/js/experiences/drawing_board/partie5.js"]
styles: ["/assets/js/experiences/drawing_board/db_css.css"]
---
{% include JB/setup %}

<div id="toolbar"><a class="pinceau tool bouton" toolstype="pinceau"> </a><a class="chenille tool bouton" toolstype="chenille"> </a><a class="cercle tool bouton" toolstype="cercle"> </a><a class="sauvegarde action bouton" toolstype="sauvegarde"> </a><a class="historique action bouton" toolstype="historique"> </a></div>
<canvas id="canvas"></canvas>
