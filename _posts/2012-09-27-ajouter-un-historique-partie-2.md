---
layout: post
title: "Ajouter un historique (partie 2)"
description: ""
category: post
tags: []
date: 2011-07-25 21:19:00
permalink: /ajouter-un-historique-partie-2
illustration: "/images/dog-standing.jpg"
attachment: "Drawing Board HTML5 - Démo étape 2 historique"
---
{% include JB/setup %}

Nous allons ajouter un historique à notre drawing board, pour permettre à l'utilisateur de revenir en arrière, si jamais il n'est pas content de son dernier tracé, à la manière du ctrl+z sur Photoshop

Vous pouvez reprendre le code que nous avons vu dans la partie 1. Une démo vous est proposée en début d'article, la touche d'historique ici est "z".

Nous voulons donc que l'utilisateur puisse retrouver chaque état précédent de son dessins, pour ça nous allons à chaque click, stocker dans un array l'état du canvas avant sa modification. Ici nous appelons la variable sauvegarde

{% highlight javascript %}
var sauvegarde = [];
{% endhighlight %}

Il nous faut ensuite définir un eventListener pour déclencher l'historique au moment souhaité. Ici nous allons mapper l'historique sur la touche du clavier "z".
Lorsque la touche "z" sera appuyée nous appelons la fonction ctrlz.

{% highlight javascript %}
window.addEventListener("keyup",ctrlz,true);
{% endhighlight %}

Nous avons défini le déclencheur, qui permettra de revenir en arrière, il nous reste maintenant à configurer la sauvegarde

Nous voulons qu'une sauvegarde du canvas soit effectuée avant chaque modification, pour cela, nous allons nous servir de la fonction on_mousedown
Pour qu'à chaque click, l'état actuel du canvas soit ajouté à l'historique nous allons utiliser getImageData(left-x,top-y,width,height). getImageData va enregistrer dans un array chaque pixel du canvas.
Ainsi, à chaque click, sera ajouté le dernier état du canvas à la variable sauvegarde.

Vous pouvez en apprendre plus sur getImageData [ici](https://developer.mozilla.org/en-US/docs/HTML/Canvas/Pixel_manipulation_with_canvas)

{% highlight javascript %}
sauvegarde.push(ctx.getImageData(0,0,width,height))
{% endhighlight %}

À présent, maintenant que nous avons la sauvegarde et le déclencheur, il nous faut définir la fonction ctrlz qui appliquera la sauvegarde au canvas

Dans un premier temps, nous allons vérifier que l'utilisateur a bien appuyé sur la touche "z" et pas une autre. Pour cela nous créons une variable, ici letterPressed qui contiendra la lettre pressée par l'utilisateur.
Ensuite, nous vérifions qu'elle est bien égale à "Z". Si elle l'est, alors :

- Nous définissons une variable i dont la valeur sera égale à la longueur de la sauvegarde.
- Nous vérifions ensuite que i est bien supérieure à 0, car si il n'y a plus de sauvegarde, alors, le canvas deviendra vide puisque l'array de pixel est vide.
- Si i>0, alors nous pouvons remplacer les pixels du canvas actuel par ceux de la dernière sauvegarde. Pour cela, nous faisons l'inverse de tout à l'heure où nous avions sélectionné l'image pour l'ajouter à la sauvegarde (getImageData et push()).
Nous allons donc prendre la dernière sauvegarde de l'array sauvegarde (pop()) et l'appliquer au canvas (putImageData(Imagedata, left-x, top-y)).
Remarque : comme nous savons que la sauvegarde fait les mêmes dimensions que le canvas, nous n'avons pas besoin de l'effacer, tous ses pixels seront réécrit.
- Puis pour finir, si jamais i est inférieure ou égale à 0, c'est-à-dire si il n'y a plus de sauvegarde, nous ne faisons rien, return false

{% highlight javascript %}
function ctrlz(e){
    var letterPressed = String.fromCharCode(e.keyCode);
    if(letterPressed === "Z"){
        var i = Number(sauvegarde.length);
        if(i>0){
            ctx.putImageData(sauvegarde.pop(), 0, 0);
        }
    }
}
{% endhighlight %}

Et voilà ! Ça fonctionne !!!