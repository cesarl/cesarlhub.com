---
layout: post
title: "Retirer le fond d'une image et le remplacer par une autre (canvas HTML5)"
description: ""
category: post
date: 2011-08-15 19:47:00
permalink: /retirer-le-fond-dune-image-et-le-remplacer-par-une-autre-canvas-html5
illustration: "/images/frog.jpg"
tags: []
attachment: "Remplacement de pixel canvas HTML5"
---
{% include JB/setup %}

Parce-que aujourd'hui c'est férié, mon post va être encore plus freaks que d'habitude.
Notre ami Bob est malheureux, il est enfermé dans un monde tout vert et rêve de flotter dans l'espace, entouré par des chiens, avec un beau nez rouge.
Nous on est sympa, on va le libérer. Pour ça on va l'extraire de son monde, et le transférer dans un nouveau.
Pour le dire autrement, nous allons chercher à remplacer le fond d'une image par une autre image.

Remarque : pour nos amis sur Firefox 5, j'ai du retirer dans la démo l'ombre portée (qui noirci tout le canvas) et notre Bob disparait au bout d'une demie rotation sur FF5, FF6 et Aurora. Chrome, Safari (et même Opera) fonctionnent parfaitement.

Nous avons donc 2 mondes :
{% highlight javascript %}
var width = 428;
var height = 320;

var bobsWorld = new Image();
bobsWorld.src = "files/green.png";

var bobsDream = new Image();
bobsDream.src = "files/dog.png";

var bobGreen = document.getElementById("canvas");
bobGreen.width = width;
bobGreen.height = height;
var ctx = bobGreen.getContext("2d");
bobsDream.addEventListener('load', launchApp, false);
{% endhighlight %}
![picture alt](http://www.cesar.lc/sites/www.cesar.lc/files/11/08/green.png)
![picture alt](http://www.cesar.lc/sites/www.cesar.lc/files/11/08/dog.png)

Pour extraire Bob de son monde, nous allons devoir analyser chacun de ses pixels et supprimer tous les pixels verts. Pour ça nous devons d'abord enregistrer dans la variable <code>pixelArray</code> l'ensemble des pixels du monde de Bob.
{% highlight javascript %}
var pixelArray = ctx.getImageData(0,0,width,height);
{% endhighlight %}

Nous allons maintenant chercher tous les pixels du même vert que celui du monde de Bob et les rendre transparent, afin de les faire disparaître.

{% highlight javascript %}
for(i=0, max = pixelArray.data.length; i<\max; i++){
    //si red >= 10 et que green >= 10 et que blue <=10
    if(pixelArray.data[i] >= 10 && pixelArray.data[i+1] >= 10 && pixelArray.data[i+2] <= 3){
        //alors ce pixel sera transparent
	pixelArray.data[i+3] = 0;
    }
}
{% endhighlight %}

Notre Bob est maintenant dans un état transitoire, entre son ancien monde et le nouveau. Mais nous n'allons pas le transférer tout de suite dans son nouveau monde. Pourquoi ? Parce que nous avons travaillé avec, souvenons nous les données de l'image et non pas l'image elle-même... Oula, c'est pas très clair ce que je raconte. En d'autres termes : pour obtenir les données de l'image de Bob sur fond vert, nous avons imprimé l'image sur le canvas (avec <code>drawImage()</code>) puis nous en avons extrait les données (avec <code>getImageData()</code>). Or, n'oublions pas que Bob veut flotter dans l'espace, donc tourner sur lui-même, et qu'en l'état actuel des choses, si nous imprimons notre nouveau Bob, nous devrons utiliser <code>putImageData()</code>. Et putImageData(), ne tiens pas compte des transformations effectuées sur le canvas (translate, rotate ...). Il nous faut donc d'abord repasser notre imageData en image pour ensuite transférer Bob dans son nouveau monde avec drawImage().

{% highlight javascript %}
//nous imprimons notre nouveau Bob
ctx.putImageData(pixelArray,0,0);
//nous lui dessinons son joli nez rouge au passage
ctx.fillStyle = "rgb(255,0,0)";
ctx.arc(204,123,15,0,360*Math.PI/180,false);
ctx.fill();
{% endhighlight %}

Reste maintenant à créer la nouvelle image :
{% highlight javascript %}
var bobTransparent = new Image();
bobTransparent.src = canvas.toDataURL();
{% endhighlight%}

Et maintenant, libérons Bob !
Pour ça commençons par définir les variables de son nouveau monde
{% highlight javascript %}
//pour la rotation
var rot = 0;
//pour la fausse 3D
var mat = 0;
var dif = 0.01;
{% endhighlight %}
C'est parti Bob, nous te libérons
{% highlight javascript %}
setInterval(andBobWillBeHappyForEver, 33);
function andBobWillBeHappyForEver(){
  rot++
  mat = mat+dif;
  //nous imprimons le fond avec les chiens
  ctx.drawImage(bobsDream,0,0);
  //ici nous ajoutons un ombre
  ctx.shadowColor = "rgba(0,0,0,0.4)";
  ctx.shadowOffsetX = ctx.shadowOffsetY = 30;
  ctx.shadowBlur = 20;
  ctx.save();
  //nous appliquons une rotation à notre canvas de +1 degré à chaque intervalle
  ctx.translate(width/2,height/2);
  ctx.rotate(rot*Math.PI/180);
  //nous appliquons une transformation à notre canvas pour que Bob ait l'impression de flotter dans l'espace
  ctx.transform(1,-mat,-mat,1,0,0)
  ctx.translate(-width/2,-height/2);
  //Bob is here !
  ctx.drawImage(bobTransparent,0,0);
  ctx.restore();
  if (rot == 360){rot = 0};
  if (mat >= 1){dif = -0.01}
  if(mat <= 0){dif = 0.01}
}
{% endhighlight %}

Le code est relativement simple ci-dessus. Juste une remarque :
Nous utilisons ici <code>transform()</code>, qui modifie la matrix du canvas. Pour être honnête, c'est très obscure pour moi, et j'ai vraiment du mal à le maitriser. Je ne me risquerai pas à vous l'expliquer, mais si vous voulez en savoir plus je vous conseille :

- _Javascript ,The Definitive Guide_, 6th Edition, de David Flanagan - Chez O'reilly. Page 637 à 642
- [_Ouch ! Ma tête_](http://www.useragentman.com/blog/2011/01/07/css3-matrix-transform-for-the-mathematically-challenged/")

{% highlight javascript %}
var width = 428;
var height = 320;
var bobsWorld = new Image();
bobsWorld.src = "/sites/www.cesar.lc/themes/cesarlc/experiences/greenscreen/files/green.png";

var bobsDream = new Image();
bobsDream.src = "/sites/www.cesar.lc/themes/cesarlc/experiences/greenscreen/files/dog.png";

bobsDream.addEventListener('load', launchApp, false);

var bobGreen = document.getElementById("canvas");
bobGreen.width = width;
bobGreen.height = height;
var ctx = bobGreen.getContext("2d");

function launchApp(){
  ctx.drawImage(bobsWorld, 0,0);

  var rot = 0;
  var mat = 0;
  var dif = 0.01;
  var pixelArray = ctx.getImageData(0,0,width,height);

  for(i=0, max = pixelArray.data.length; i<\max; i++){
    if(pixelArray.data[i] >= 10 && pixelArray.data[i+1] >= 10 && pixelArray.data[i+2] <= 3){
      pixelArray.data[i+3] = 0;
    }
  }

  ctx.putImageData(pixelArray,0,0);
  ctx.fillStyle = "rgb(255,0,0)";
  ctx.arc(204,123,15,0,360*Math.PI/180,false);
  ctx.fill();

  var bobTransparent = new Image();
  bobTransparent.src = canvas.toDataURL();

  setInterval(andBobWillBeHappyForEver,33)

  function andBobWillBeHappyForEver(){
    rot++
    mat = mat+dif;
    ctx.drawImage(bobsDream,0,0);

    ctx.shadowColor = "rgba(0,0,0,0.4)";
    ctx.shadowOffsetX = ctx.shadowOffsetY = 30;
    ctx.shadowBlur = 20;

    ctx.save();
    ctx.translate(width/2,height/2);
    ctx.rotate(rot*Math.PI/180);
    ctx.transform(1,-mat,-mat,1,0,0)
    ctx.translate(-width/2,-height/2);
    ctx.drawImage(bobTransparent,0,0);
    ctx.restore();
    if (rot == 360){rot = 0};
    if (mat >= 1){dif = -0.01};
    if(mat <= 0){dif = 0.01};
  }
}
{% endhighlight %}
