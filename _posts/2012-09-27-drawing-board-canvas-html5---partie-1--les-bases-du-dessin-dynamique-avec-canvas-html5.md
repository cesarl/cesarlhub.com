---
layout: post
title: "Drawing Board Canvas HTML5   Partie 1 : Les bases du dessin dynamique avec canvas HTML5"
description: ""
category: post
tags: []
date: 2011-07-24 17:40:00
permalink: /drawing-board-canvas-html5-partie-1-les-bases-du-dessin-dynamique-avec-canvas-html5
illustration: "/images/walking_cat.png"
attachment: "Drawing Board HTML5 - Démo étape 1"
---
{% include JB/setup %}

Dans cette première partie nous allons nous intéresser uniquement à l'outil pinceau. Nous voulons que l'utilisateur puisse, comme sur Photoshop, dessiner sur l'image à l'aide de sa souris. Pour arriver à ce résultat, nous allons utiliser la position de la souris et "l'outil" line de l'api Canvas, pour tracer des lignes entre chaques nouvelles positions de la souris sur la surface du canvas.

Je me suis appliqué à commenter le code autant que possible ce qui devrait faciliter sa compréhension. Vous pouvez le retrouver dans son intégralité en fin d'article.
Une démo est aussi disponible en début d'article.

La première fonction englobant la totalité de l'application est déclenchée une fois la page loadée.
Nous y définissons ensuite les variables dont nous aurons besoin tout au long du développement, parmi lesquelles, la balise canvas, les dimensions du canvas, son contexte, la largeur du tracé ainsi que sa couleur, l'image de fond ...
La variable `ink` fera office "robinet" ; false bloquera les fonctionnalités du drawing board, `true` permettra à l'utilisateur de dessiner sur le canvas. Il faudra donc que le bouton de la souris soit enfoncé au niveau de la surface du canvas pour que `ink = true`.
Les variables `lastPos` et `mousePos` nous servirons à tracer chaque segment du tracé, du point `lastPos.x lastPos`.y au point `mousePos.x mousePos.y`.
Remarquons aussi que nous ajoutons un eventListener qui ne lancera les fonctionnalités de dessin qu'une fois l'image chargée.

{% highlight javascript %}
//largeur du canvas
var width = 910;

//hauteur du canvas
var height = 561;

// la balise <canvas id="canvas"></canvas>
var canvas = document.getElementById("canvas");

// j'applique au canvas la largeur définie plus haut
canvas.width = width;

// j'applique au canvas la hauteur définie plus haut
canvas.height = height;

//j'applique le rendering context ==> plus d'infos ici : http://s4r.fr/qlV95E
var ctx = canvas.getContext('2d');

//dernière position de ma souris sur la canvas
var lastPos = {x:0, y:0};

//position actuelle de ma souris sur le canvas
var mousePos = {x:0, y:0};

//la souris dessinera sur le canvas une fois l'ink true, si ink = false, alors rien ne se passe au survol de la souris
var ink = false;

//définit la couleur de mon tracé
var colorLine = "#BABA12"

//définit la largeur de mon tracé
var lineWidth = 10

// je crée une variable image pour définir l'image de fond
var myImage = new Image();

//je définis le chemin de l'image
myImage.src = "images/ash1.jpeg";

//une fois l'image chargée, je lance la fonction
myImage.addEventListener('load', imageIsLoaded, false);
{% endhighlight %}

Une fois l’image loadée, nous la printons sur le canvas. Ici le canvas et l’image ont les mêmes dimensions, donc nous nous contentons de la placer en x:0, y:0.

L’image est là, nous pouvons donc commencer à dessiner dessus, pour cela nous ajoutons les principaux eventListener qui permettrons de suivre les interactions de l’utilisateur avec le canvas. Ces derniers correspondrons donc :
- au click de la souris,
- au mouvement de la souris,
- au “dé-click” de la souris

Tout ça sur la surface du canvas uniquement puisque c’est ce qui nous intéresse ici.

{% highlight javascript %}
//l'image est prête à être printée sur canvas
function imageIsLoaded(){
    //l'image est printée sur le canvas http://s4r.fr/qkl2pB
    ctx.drawImage(myImage, 0,0);

    //au click sur le canvas
    canvas.addEventListener("mousedown", on_mousedown, false);

    //quand la souris bouge sur la surface du canvas
    canvas.addEventListener("mousemove", on_mousemove, false);

    //quand le bouton de la souris est levé
    canvas.addEventListener("mouseup", on_mouseup, false);
}
{% endhighlight %}

Maintenant il nous reste à définir les fonctions qui interviendront à chaque click et mouvement de souris. Elles nous permettrons de calculer la position exacte de la souris sur le canvas.

Commençons par le scroll. En effet, nous le verrons plus tard nous allons calculer la position de la souris par rapport à la fenêtre, or si celle-ci à été scrollée, il en résultera un décalage, donc, pour corriger ça, il nous faut calculer la valeur en pixel du scroll vertical et horizontal. Pour cela, le code parle encore une fois de lui-même. Juste une petite remarque, nous utilisons ici un double pipes || (or) car tous les navigateurs ne répondent pas à la même fonction, pour certain c'est `document.scrollLeft`, pour d'autre `window.pageXOffset`.

{% highlight javascript %}
function scrollPosition(e) {
    return {
        //scroll s/ l'axe des x : majorité des navigateurs || pour les autres
        x: document.scrollLeft || window.pageXOffset,
        //scroll s/ l'axe des y : majorité des navigateurs || pour les autres
        y: document.scrollTop || window.pageYOffset
    };
}
{% endhighlight %}

Maintenant que nous pouvons connaître le niveau de scroll de la page, nous pouvons calculer la position exacte de la souris sur le canvas.
Allez ! On est dimanche ! Je vous fais un dessin !

!["Offset"](/images/offset.jpeg)

{% highlight javascript %}
function calculateMouseCoord(e){
    return{
        // la position de la souris s/ l'axe des x par rapport au bord gauche de la page,
        // moins la position du bord gauche du canvas par rapport au bord gauche de la page,
        // plus la valeur du scroll sur l'axe des x
        x: e.clientX - canvas.offsetLeft + scrollPosition().x,
        //pareil que pour les x, mais sur l'axe des y
        y: e.clientY - canvas.offsetTop + scrollPosition().y
    }
}
{% endhighlight %}

Allez ! On n'est pas loin de la fin, on a presque tout préparé, on va bientôt pouvoir jouer un peu avec l'api canvas

On commence par le click. Comme discuté plus haut, le click sur la surface du canvas va activer ce que nous avons appelé `ink`, l'encre va donc pouvoir s'écouler car `ink = true`
Nous updatons ensuite la variable lastPos pour qu'elle porte la position de la souris sur le canvas au moment du click, pour cela nous appelons la fonction que nous venons d'écrire `calculateMouseCoord`. Si la souris est pile au centre du canvas par exemple, cela donnera lastPos = {x:455, y:280};(en vérité, il n'y a pas de vrai milieu dans cet exemple puisque les demis pixels n'existent pas, donc ce sera 280 ou 281 ... bref, c'est pas bien important).

{% highlight javascript %}
//quand le bouton de la souris est appuyé
function on_mousedown(e){
    //l'encre se trouve activé, ce qui autorisera le dessin sur le canvas
    ink = true;
    //nous calculons la position de la souris au même moment sur le canvas
    lastPos = calculateMouseCoord(e);
}
{% endhighlight %}

Voyons ensuite ce qu'il se passe quand la souris bouge sur la surface du canvas.
Dans un premier temps il nous faut vérifier que l'utilisateur a bien le bouton de sa souris enfoncé, si nous ne le faisons pas alors ça gribouillerait notre belle image au simple survole de la souris sur le canvas. Il faudra donc que ink soit égale à true pour qu'il se passe quelque chose sur le canvas.

Nous allons procéder comme pour lastPos au moment du click, mais cette fois-ci pour mousePos, c'est-à-dire pour la position actuelle de la souris au moment du mouvement.
Puis, tout frissonnant de hâte et de plaisir, nous lançons la fonction tant attendue, celle grâce à qui notre beau dessin prendra vie : drawOnMyCanvas() !!!

{% highlight javascript %}
//quand la souris bouge sur la surface du canvas
function on_mousemove(e){
    //si jamais l'encre est désactivée (=false) alors rien ne se passe, car l'utilisateur n'a pas click
    if(ink == false){return false}
    //autrement, si l'utilisateur a clické
    else{
        // nous calculons la position x et y de la souris sur le canvas (à chaque fois que la souris bouge s/ sa surface)
        mousePos = calculateMouseCoord(e);
        //nous appelons la fonction drawOnMyCanvas qui appliquera le dessins s/ le canvas
        drawOnMyCanvas(e);
    }
}
{% endhighlight %}

N'oublions surtout pas le unclick, sans ça même une fois le bouton relâché, la souris continuerait de peindre sur le canvas, catastrophe. Pour éviter cela, il nous faut repasser notre variable ink en false.

{% highlight javascript %}
function on_mouseup(e){
    //l'encre est coupée pour ne plus dessiner s/ le canvas mm si la souris bouge sur sa surface
    ink = false;
}
{% endhighlight %}

C'est parti pour le dessin ! Encore une fois le code parle de lui-même, et j'y ai ajouté des liens pour ceux qui n'auraient jamais jusqu'ici fait un peu de canvas.
Dans l'ordre on ouvre un nouveau tracé avec beginPath(). On le prépare à recevoir certaines propriétés graphiques : couleur strokeStyle, largeur lineWidth, type d'extrémités lineCap (ici arrondies - essayez square, c'est beaucoup moins joli) et ses angles lineJoin. Puis, nous plaçons notre pinceau virtuel aux mêmes coordonnées que celles de la dernière position de la souris avec moveTo(x,y) (aux coordonnées du click si il s'agit du premier tracé depuis que nous avons le bouton de la souris enfoncé). Enfin nous traçons notre ligne jusqu'à la position actuelle de la souris avec lineTo(x,y). Et pour finir en beauté nous appliquons à la ligne les propriétés graphiques définies plus haut, elle devient alors visible et s'imprime sur le canvas.

Mais ce n'est pas tout, imaginons que nous bougions encore notre souris, il nous faut encore updater la variable lastPos, sinon le second tracé se fera entre la position du click et la nouvelle position de la souris. Or, nous, ici, nous voulons faire des dessins, pas des étoiles, c'est pourquoi il nous faut remplacer les anciennes cordonnées de lastPos par celles de la position actuelle de la souris une fois le ligne tracée. Bon allez, encore un dessin.

!["update"](/images/update.jpeg)

Et voillllllà ! Ça marche !
Vous pouvez copier coller le code en fin d'article pour essayer chez vous.

Bon aller suite au prochain épisode, moi je vais boire de la bière