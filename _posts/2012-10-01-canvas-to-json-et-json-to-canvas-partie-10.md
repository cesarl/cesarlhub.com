---
layout: post
title: "Canvas to Json et Json to canvas (partie 10)"
description: ""
category: post
date: 2011-08-05 19:43:00
permalink: /canvas-json-et-json-canvas-partie-10
illustration: "/images/phoque.jpg"
attachment: "Demo canvas to json - json to canvas"
tags: []
---
{% include JB/setup %}

Nous allons voir ici comment convertir les tracés effectués sur notre drawing board en json, et comment à partir du json redessiner sur le canvas de manière animée.

Jusqu'ici toutes les fonctionnalités de tracé, d'enregistrement et d'exportation que nous avons explorées se basaient uniquement sur les pixels imprimés sur le canvas. En d'autres termes, nous ne mémorisions que les pixels ; l'historique n'était rien d'autre qu'une série d'instantanés du canvas. Ici, nous allons essayer d'enregistrer les coordonnées et propriétés du tracé en json.

Nous n'irons ici pas très loin dans l'application de cette nouvelle méthode d'enregistrement. Nous allons simplement voir comment enregistrer le tracé en json, et comment à partir de ce dernier redessiner sur notre canvas.

**Remarque** : j'utilise ici du local storage html5 pour mémoriser le dernier dessin que vous avez fait sur le canvas. Je ne l'explique pas dans cet artcile, vous pouvez regarder mon code source.

Comme d'habitude ça ressemble beaucoup à tout les autres exemples que l'on a pu voir das les articles précédents.

Nous allons ajouter deux variables :

- `strike`, qui contiendra toutes les informations de chaque tracé (mousedown --> mouseup).
- `path` qui contiendra les cordonnées de chaque segment de tracé et sa couleur. (mousemove)

Nous commençons donc par ajouter en tête de l'array path la couleur du tracé à chaque click.

{% highlight javascript %}
function on_mousedown(e){
    if(ucandraw == false){
        ucandraw = true;
    }
    ink = true;
    lastPos = calculateMouseCoord(e);
    path.push({color:colorLine});
}
{% endhighlight %}

La variable `ucandraw`, que nous retrouverons plus tard servira à effacer le dessin par défaut.

Ensuite, à chaque mousemove, en plus de dessiner sur notre canvas, nous ajouter à path un array line contenant les coordonnées du segment que nous venons de tracer.

{% highlight javascript %}
path.push({line:{x:lastPos.x, y:lastPos.y, toX:mousePos.x, toY:mousePos.y}});
{% endhighlight %}

Enfin onmouseup, nous ajoutons toutes les données collectées dans path à notre array `strike`
Puis, nous vidons notre variable `path` afin qu'elle puisse contenir les informations du prochain tracé.

Nous pouvons alors lancer la fonction `toJson()` qui convertira notre array `strike` en json.

{% highlight javascript %}
strike.push({nbr:path});
path = [];
toJson();
{% endhighlight %}

Notre array `strike` au "dé-click" de la souris (= on_mouseup) ressemblera par exemple à ça :

- path1 (premier tracé)
  - color
  - nbr 1
    - x
    - y
    - toX
    - toY
  - segment 2
  - segment 3
  - ...
- Path2
  - color
  - segment 1
  - segment 2
  - ...

Voyons maintenant la fonction toJson dont le rôle est de convertir strike au format json.
Je me suis inspiré pour ça du code source de Hakim El Hattab pour son application [Sketch](http://www.chromeexperiments.com/detail/sketch/).

{% highlight javascript %}
function toJson(){
    var o = '{';
    o += '"value":[';
    //pour chaque tracé (mousedown --> mouseup)
    for(var l=0; l<strike.length; l++){
	o += '{"ligne":[';
	//nous renseignons la couleur
	o += '{"color":"'+ strike[l].nbr[0].color +'"},'
	//pour chaque segment du tracé
	for(var n=1; n<strike[l].nbr.length; n++){
	    o += '{'
	    o += '"x":' + strike[l].nbr[n].line.x + ',';
	    o += '"y":' + strike[l].nbr[n].line.y + ',';
	    o += '"toX":' + strike[l].nbr[n].line.toX + ',';
	    o += '"toY":' + strike[l].nbr[n].line.toY;
	    o += '}';
	    // si ce n'est pas le dernier segment du tracé, nous ajoutons alors une virgule
	    if(n != strike[l].nbr.length -1){
		o += ',';
	    }
	}
	o += (']}')
	// si ce n'est pas le dernier tracé, nous ajoutons alors une virgule
	if(l != strike.length -1){
	    o += ','
	}
    }
    o += ']}'
    // nous imprimons dans notre textarea le json généré
    $("#textarea").empty().text(o);
}
{% endhighlight %}

Et voilà ! Nous avons notre json tout prêt à être enregistré sur le serveur ! (nous traiterons pas ce point ici - nous utilisons pour cette démo le local storage - cf code source "exemple.js")
Nous allons appeler un joli dessin (en json) que je vous ai préparé pour cet article. Nous allons donc essayer ici de le décrypter et de le dessiner sur le canvas.
Pour cela, nous allons commencer par appeler notre fichier :

{% highlight javascript %}
$.getJSON('default.json', function(){
  var p=0;
  var q = 1;
});
{% endhighlight %}

La variable `p` servira de compteur pour les tracés et `q` de compteur pour les segments des tracés (nous commençons à 1, car nous avons placé en première position la couleur du tracé).
On appelle ensuite la fonction `drawJson()` qui dessinera le json tracé par tracé et segment par segment

{% highlight javascript %}
$.getJSON('default.json', function(data){
    var data = data;
    var p=0;
    var q = 1;

    drawJson();

    function drawJson(){
	//nous definissons les propriétés graphiques de notre canvas
	bgctx.lineWidth = lineWidth;
	bgctx.lineCap = 'round';
	bgctx.lineJoin = 'round';

	//si p est inférieur ou égale au nombre de tracé
	if(p<data.value.length){
	    //>
	    //si dans le tracé p, q est inférieur ou égal au nombre de segments
	    if(q<data.value[p].ligne.length){
		//>
		//nous appliquons notre couleur
		bgctx.strokeStyle = data.value[p].ligne[0].color;
		bgctx.beginPath();
		//puis nous traçons le segement
		bgctx.moveTo(data.value[p].ligne[q].x, data.value[p].ligne[q].y)
		bgctx.lineTo(data.value[p].ligne[q].toX, data.value[p].ligne[q].toY);
		bgctx.stroke();
		bgctx.closePath();
		//une fois le premier segment tracé nous ajoutons 1 à q pour tracer le suivant
		q++

		//si la variable ucandraw = false, c'est à dire si l'utilisateur
		//n'a pas clické sur le canvas alors que celui-ci était en train d'être illustré
		//à partir des informations du json.
		//Alors nous relançons la fonction drawJson pour tracer le segment suivant.


		if(ucandraw == false){setTimeout(drawJson,10)}
		//sinon c'est que l'utilisateur a cliqué, alors nous arrêtons l'animation
		//et effacons le canvas
		else{
		    bgctx.rect(0,0,width,height);
		    bgctx.fill();
		}
		//si q est supérieur au nombre de segment dans le tracé
	    } else{
		//nous réinitialisons la valeur de q
		q = 1;
		//nous ajoutons 1 à p pour passer au segment suivant
		p++
		setTimeout(drawJson,10);
	    }
	    //si p est supérieur au nombre de tracé
	    //alors c'est que le json est entièrement tracé
	    //et nous pouvons autoriser l'utilisateur à dessiner

	} else {
	    ucandraw = true;
	    return false
	}
    }
});
{% endhighlight %}


