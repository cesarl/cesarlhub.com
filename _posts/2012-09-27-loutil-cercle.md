---
layout: post
title: "L'outil cercle"
description: ""
category: post
tags: []
date: 2011-07-26 21:19:00
permalink: /loutil-cercle-partie-5
illustration: "/images/cat_glasses.jpg"
attachment: "Drawing Board HTML5 - Démo étape 5 - cercle"
---
{% include JB/setup %}

Maintenant, rajoutons un outil cercle en se servant du code des chapitres précédents. Ici je l'ai un peu refondu pour que ce soit plus pratique, j'y ai rajouté une toolbar et du jquery. Dans l'idée rien n'a véritablement changé, et les raccourcis clavier "Z" et "S" fonctionnent encore.
_Notez aussi que j'y ai ajouté du Jquery._

Nous voulons donc tracer des cercles, à priori ça semble assez facile, il nous suffit d'utiliser arc que nous avons rencontré dans la partie précédente. Seulement, il ne faut pas oublier que le canvas est imprimé de manière immédiate, c'est-à-dire qu'à chaque modification, il est entièrement redessiné pixel par pixel. C'est pourquoi par exemple, notre historique ne mémorise pas uniquement le dernier tracé mais la totalité du canvas. Ce mode de dessin "immédiat" propre au canvas risque de nous poser problème dans le cas de l'outil cercle. Pourquoi ? Je vous fais un dessin et je vous explique.

!["steps"](/images/steps.jpg)

Nous voulons donc tracer un cercle :

- Nous enfonçons le bouton de la souris
- Du centre, nous glissons notre souris jusqu'à l'étape 1, un cercle apparait alors.
- Nous éloignons encore notre souris du centre du cercle, le cercle alors s'agrandit.
- Puis, nous décidons qu'il est un peu trop gros et rapprochons notre souris du centre du cercle. Mais, rien ne se passe, le cercle reste au stade de l'étape 2, nous ne pouvons pas le rétrécir.

Pourquoi pouvons nous agrandir le cercle, mais ne pouvons nous pas le rétrécir ? Car en réalité, à chaque fois que nous bougeons notre souris nous dessinons sur le canvas, par conséquent, le plus petit cercle de l'étape 3 "existe", seulement il est imprimé sur le plus grand.
Il nous faut donc effacer le dernier cercle à chaque mouvement de la souris pour pouvoir "rétrécir" notre cercle. Pour ça, nous allons tout simplement utiliser l'historique que nous avons développé dans la partie 2.

Ainsi, à chaque mouvement de souris avec l'outil cercle :
Nous définissons le rayon du cercle comme égal à la distance entre la position du click initial et celle du curseur.

{% highlight javascript %}
circleRadius = Math.abs(lastPos.x - mousePos.x);
{% endhighlight %}

Puis nous créons la variable i égale à la position de la dernière sauvegarde dans l'array du `sauvegarde`.

{% highlight javascript %}
var i = Number(sauvegarde.length) -1;
{% endhighlight %}

Nous remplaçons ensuite le canvas par la sauvegarde. Le cercle précédent se trouve donc "effacé", puisque la totalité du canvas est réécrite à partir de la sauvegarde.

{% highlight javascript %}
ctx.putImageData(sauvegarde[i], 0, 0);
{% endhighlight %}

Nous pouvons alors tracer notre cercle !

{% highlight javascript %}
ctx.strokeStyle = colorLine;
ctx.strokeWidth = lineWidth;
ctx.fillStyle = "#D0145A";
ctx.beginPath();
ctx.arc(lastPos.x,lastPos.y, circleRadius, Math.PI*2, false);
ctx.fill();
ctx.closePath();
{% endhighlight %}

Et la magie s'opère !