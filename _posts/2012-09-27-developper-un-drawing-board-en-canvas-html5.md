---
layout: post
title: "Développer un Drawing Board en Canvas HTML5"
description: ""
category: post
tags: []
date: 2011-07-24 21:19:00
permalink: /developper-un-drawing-board-en-canvas-html5
illustration: "/images/gouru.jpg"

---
{% include JB/setup %}

Je me suis dernièrement penché sur l'api Canvas HTML5, j'avais envi de développer un drawing board, parce que... c'est drôle. Je me suis donc plongé dedans et je vous partage ici mes découvertes.

Je vais essayer, à travers une série de posts, de vous montrer comment je m'y prends. Je ne doute pas que cela puisse être mieux codé - je découvre presque en même temps que vous, n'hésitez donc pas à me faire part d'améliorations possibles.

Au programme pas mal de choses : dessiner des traits, des formes, ajouter un historique, jouer avec la transparence etc etc.
Je n'expliquerais pas dans le détail l'api canvas, pour ça je vous conseille mes dernières lectures.

**Remarque** : _Il peu rester encore pas mal de bugs. Pour le moment les exemples fonctionnent sur FF, Chrome et Safari._

### Lectures

- [Mozilla Developer Network] (https://developer.mozilla.org/en/HTML/Canvas)
- [HTML5 Canvas - O'Reilly Media] (http://oreilly.com/catalog/0636920013327/)
- [Canvas Pocket Reference - O'Reilly Media] (http://oreilly.com/catalog/0636920016045/)
- [JavaScript: The Definitive Guide, Sixth Edition - O'Reilly Media] (http://oreilly.com/catalog/9780596805531/)

Vous pouvez aussi vous amuser à inspecter le code source de :

- [Sketch, d'Hakim El Hattab] (strip_html)
- [Canv.as] (http://www.canv.as)

Et quelques tutos / posts :
- [Dev.Opera] (http://dev.opera.com/articles/view/html5-canvas-painting/)
- ...la flemme de parcourir tout mon historique - Google est votre ami

### Liste des articles

- Drawing Board Canvas HTML5 - Partie 1 : Les bases du dessin dynamique avec canvas HTML5
- Ajouter un historique (partie 2)
- Sauvegarde l'image - Canvas to Img (partie 3)
- Ajouter un brosse custom à notre Canvas Drawing Board (partie 4)
- L'outil cercle (partie 5)
- Outil rectangle et carré (partie 6)
- Contours, couleur et transparence (partie 7)
- Outil crop et introduction aux calques (partie 8)
- Zoom et color picker (partie 9)
- Canvas to Json et Json to canvas (partie 10)
- Outil polygone pour notre drawing board en HTML5 (partie 11)
