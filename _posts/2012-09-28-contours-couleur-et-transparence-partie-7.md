---
layout: post
title: "Contours, couleur et transparence (partie 7)"
description: ""
category: post
tags: []
date: 2011-07-28 19:47:00
permalink: /contours-couleur-et-transparence-partie-7
illustration: "/images/snow-squirrel.jpg"
attachment: "Drawing Board HTML5 - Démo étape 7 - Contours, couleur et transparence"
---
{% include JB/setup %}

Nous allons ajouter à notre drawing board des options d'épaisseur de contour, d'opacité et de couleur.

Pour cela, nous allons avoir besoin d'un second élément canvas qui nous servira d'aperçu.
Je ne vais pas m'attarder à décrire le html de la sidebar, remarquons simplement que j'ai utilisé [html5slider](http://frankyan.com/labs/html5slider/) pour rendre compatible les inputs de type range avec Firefox. Pour le color picker j'ai utilisé [JSColor](http://jscolor.com/)

Une fois le document chargé, nous lançons la fonction joliment appelée propertiesPlease qui va aller chercher toutes les valeurs par défaut dans le html (couleurs, épaisseur, opacité) pour les appliquer ensuite sur notre canvas d'aperçu, sous la forme d'un carré.

{% highlight javascript %}
function propertiesPlease(){
    fillOp = $("#opacity-fill").val()/100; //opacité du fond.
    strokeOp = $("#opacity-stroke").val()/100; //opacité du contour
    lineWidth = Number($("#stroke-width").val()) //largeur du contour
    fillCol = document.getElementById("fill-color"); //input de couleur de fond
    strokeCol = document.getElementById("stroke-color"); //input de couleur du contour
    colorFill = "rgba("+Math.round(fillCol.color.rgb[0]*255)+","+Math.round(fillCol.color.rgb[1]*255)+","+Math.round(fillCol.color.rgb[2]*255)+","+fillOp+")"; //nous convertissons la couleur de fond en rgba -- pour en savoir plus http://s4r.fr/nK8KNS
    colorLine = "rgba("+Math.round(strokeCol.color.rgb[0]*255)+","+Math.round(strokeCol.color.rgb[1]*255)+","+Math.round(strokeCol.color.rgb[2]*255)+","+strokeOp+")"; //nous convertissons la couleur de contour en rgba -- pour en savoir plus http://s4r.fr/nK8KNS
    isStroke = $("#stroke").attr("checked"); // les contours sont activés ou désactivés
    isFill = $("#fill").attr("checked"); // couleur de fond est activée ou désactivée

    //nous appliquons les valeurs récoltée ci-dessus à notre canvas d'aperçu
    minictx.strokeStyle = colorLine;
    minictx.lineWidth = lineWidth;
    minictx.fillStyle = colorFill;
    minictx.clearRect(0,0,miniWidth,miniHeight);

    if(isFill === "checked"){ //pour le fond
        minictx.beginPath();
        minictx.rect(70,40, 60,60);
        minictx.fill();
        minictx.closePath();
        $("#fill-box").fadeIn();
    } else {$("#fill-box").fadeOut()}

    if(isStroke === "checked"){ //pour les contours
        minictx.beginPath();
        minictx.rect(70-(lineWidth/2),40-(lineWidth/2), 60+(lineWidth*1),60+(lineWidth*1));
        minictx.stroke();
        minictx.closePath();
        $("#stroke-box").fadeIn();
    } else {$("#stroke-box").fadeOut();}
}
{% endhighlight %}

Mais pourquoi dans le code ci-dessus nous dessinons un rectangle pour les contours et un pour le fond. Ne pourrions-nous pas tout simplement ne tracer qu'un rectangle avec bordure et fond ?
Haha ! Jusqu'ici nous n'avions pas à nous poser la question car nous n'utilisions pas de transparence, nous ne pouvions donc pas remarquer que les contours en canvas (stroke) sont centrés sur le tracé et non pas à l'extérieur. Un dessin pour illustrer tout ça.

![contours](/images/contours.jpg)

Il faut donc tracer 2 fois la forme si nous voulons que les bordures épousent les contours du rectangle. Il nous faudra donc appliquer ces modifications sur tous les outils du drawing board. Vous pouvez copier coller le code en fin d'article pour vous rendre compte de la différence, ou comparer les deux snaps ci-dessous.

![cicle animal 1](/images/circle-animal1.jpg)
![cicle animal 2](/images/circle-animal2.jpg)

Si vous avez joué avec la démo (en début d'article), vous avez sans doute aussi remarqué que l'outil pinceau avec de la transparence ne rendait pas génial. Et oui, encore une chose dont nous ne nous rendions pas compte dans les précédents articles. Pourquoi la transparence n'est pas uniforme ? Look at my dessin.

![linecap](/images/linecap.jpg)

Le dessin est super clair ? Cool ! Pas besoin d'expliquer
Au fait, une super cheat sheet : [ici](http://blog.nihilogic.dk/2009/02/html5-canvas-cheat-sheet.html). (n'oubliez pas de jouer à Mario en passant - avec les flèches de directions)
Comment allons-nous pouvoir y remédier ? Pour le moment je ne sais pas. Peut-être en enregistrant des points puis en traçant nos lignes d'un seul tenant... quoi qu'il en soit, c'est possible et on peu le voir sur différents sites, reste à comprendre comment. (update : nous nous approchons de ce principe partie 10 )

Bref, revenons-en à nos bordures qui se centrent, et tentons de les appliquer aux différents outils.

Pour le cercle, c'est assez simple, il nous suffit de tracer un second cercle en ajoutant à son rayon la moitié de la largeur du contour.

{% highlight javascript %}
ctx.arc(lastPos.x,lastPos.y, (Math.abs(distance.x)+(lineWidth/2)), Math.PI*2, false);
{% endhighlight %}

Pour l'outil rectangle et carré, ça se complique un peu. Mon premier réflexe a été d'écrire ça :

{% highlight javascript %}
ctx.beginPath();
ctx.rect(lastPos.x-(lineWidth/2),lastPos.y-(lineWidth/2), (distance.x-lineWidth)*-1, (distance.y-lineWidth)*-1);
ctx.stroke();
ctx.closePath();
{% endhighlight %}

Ce qui fonctionne très bien lorsque l'on trace notre rectangle de haut en bas et de gauche vers la droite. Par contre, dès que l'on sort de ce schéma (par exemple : droite->gauche, bas->haut), ça ne marche plus.
_Remarque, j'ai laissé en commenté dans le code ce test pour que vous puissiez faire le test._
Ca ne marche pas car plutôt que d'agrandir le rectangle destiné à recevoir les contours, cela va le rétrécir, les contours se trouverons donc dans la forme et non pas à l'extérieur. Cf: snap ci dessous.

![fleches](/images/fleches.jpg)

Pour éviter cela, nous allons utiliser la valeur de lineWidth différemment pour l'axe des x et celui des y. Nous allons commencer par définir une nouvelle variable : `var decalage = {x:lineWidth, y:lineWidth}`.
Rappelons d'abord que le mouvement de gauche à droite et de haut en bas donne pour résultat de distance une valeur négative, c'est pourquoi toujours avec l'outil rectangle nous inversons distance.
Il va donc nous falloir inverser le décalage lorsque la distance sera positive pour qu'elle. ... j'ai vraiment du mal à l'expliquer. Regardez le code, amusez-vous à tripoter les valeurs, et vous comprendrez.
Ce qui donne donc :

{% highlight javascript %}
var decalage = {x:lineWidth, y:lineWidth}
if(distance.x > 0){
    decalage.x = -decalage.x;
}
if(distance.y > 0){
    decalage.y = -decalage.y;
}
ctx.beginPath();
ctx.rect(lastPos.x-(decalage.x/2),lastPos.y-(decalage.y/2), distance.x*-1 + decalage.x, distance.y *-1 + decalage.y);
ctx.stroke();
ctx.closePath();
{% endhighlight %}



### Code en intégralité
{% highlight javascript %}
    window.addEventListener('load', canvasApplication, false);
    function canvasApplication(){
        //block selection
        document.onselectstart = function () { return false; }

        //proprietes

        var fillOp
        var strokeOp
        var fillCol
        var strokeCol
        var colorLine
        var colorFill
        var lineWidth
        var miniCanvas = document.getElementById("apercu-proprietes");
        var minictx = miniCanvas.getContext('2d');
        var miniWidth = miniCanvas.width;
        var miniHeight = miniCanvas.height;
        var isStroke;
        var isFill;
             
        propertiesPlease();
         
        function propertiesPlease(){
            fillOp = $("#opacity-fill").val()/100;
            strokeOp = $("#opacity-stroke").val()/100;
            lineWidth = Number($("#stroke-width").val())
            fillCol = document.getElementById("fill-color");
            strokeCol = document.getElementById("stroke-color");
            colorFill = "rgba("+Math.round(fillCol.color.rgb[0]*255)+","+Math.round(fillCol.color.rgb[1]*255)+","+Math.round(fillCol.color.rgb[2]*255)+","+fillOp+")";
            colorLine = "rgba("+Math.round(strokeCol.color.rgb[0]*255)+","+Math.round(strokeCol.color.rgb[1]*255)+","+Math.round(strokeCol.color.rgb[2]*255)+","+strokeOp+")";
            isStroke = $("#stroke").attr("checked");
            isFill = $("#fill").attr("checked");
             
             
            minictx.strokeStyle = colorLine;
            minictx.lineWidth = lineWidth;
            minictx.fillStyle = colorFill;
            minictx.clearRect(0,0,miniWidth,miniHeight);
            if(isFill === "checked"){
                minictx.beginPath();
                minictx.rect(70,40, 60,60);
                minictx.fill();
                minictx.closePath();
                $("#fill-box").fadeIn();
            } else {$("#fill-box").fadeOut()}
            if(isStroke === "checked"){
                minictx.beginPath();
                minictx.rect(70-(lineWidth*0.5),40-(lineWidth*0.5), 60+lineWidth,60+lineWidth);
                minictx.stroke();
                minictx.closePath();
                $("#stroke-box").fadeIn();
            } else {$("#stroke-box").fadeOut();}
        }
         
        $("#proprietes input").change(function(e){
            propertiesPlease();
        });
     
        var width = 680;
        var height = 656;
        var canvas = document.getElementById("canvas");
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext('2d');
        var lastPos = {x:0, y:0};
        var mousePos = {x:0, y:0};
        var mouseDiff = {x:0, y:0};
        var ink = false;
        var myImage = new Image();
        myImage.src = "images/animal.jpg";
        myImage.addEventListener('load', imageIsLoaded, false);
        var letterPressed;
        var touchPressed;
        var sauvegarde = [];
        var outil = "pinceau";
        var action = "";
        var distance = {x:10, y:10};
        var lastCircleRadius;
        $("#toolbar").find("."+outil).addClass("active");
         
        function imageIsLoaded(){
            ctx.drawImage(myImage, 0,0);
 
            canvas.addEventListener("mousedown", on_mousedown, false);
            canvas.addEventListener("mousemove", on_mousemove, false);
            canvas.addEventListener("mouseup", on_mouseup, false);
            window.addEventListener("keyup",oneKeyUp,true);
            window.addEventListener("keydown",oneKeyDown,true);
 
        $("#toolbar .bouton").click(function(){
            if($(this).hasClass("tool")){
                $(this).addClass("active")
                $(".tool").not(this).removeClass("active");
                outil = $(this).attr("toolstype");
            };
            if($(this).hasClass("action")){
                action = $(this).attr("toolstype");
                actions();
            }
        });
        }
        function oneKeyUp(e){
            letterPressed = String.fromCharCode(e.keyCode);
            touchPressed = "";
            actions(e);
        }
        function oneKeyDown(e){
            touchPressed = e.keyCode;
        }
        function actions(){
            if(action === "historique" || letterPressed === "Z"){
                var i = Number(sauvegarde.length);
                if(i>0){
                    ctx.putImageData(sauvegarde.pop(), 0, 0);
                }
            }
         
            if(action === "sauvegarde" || letterPressed === "S"){
                var canvasToImg = canvas.toDataURL();
                window.open(canvasToImg,"nom de la fenêtre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
            }
            action ="";
            letterPressed = "";
        }
 
        function scrollPosition(e) {
            return {
                x: document.scrollLeft || window.pageXOffset,
                y: document.scrollTop || window.pageYOffset
            };
        }
        function calculateMouseCoord(e){
            return{
                x: e.clientX - canvas.offsetLeft + scrollPosition().x,
                y: e.clientY - canvas.offsetTop + scrollPosition().y
            }
        }
        function on_mousedown(e){
        sauvegarde.push(ctx.getImageData(0,0,width,height))
            ink = true;
            lastPos = calculateMouseCoord(e);
        }
        function on_mousemove(e){
            if(ink == false){return false} 
            else{
                mousePos = calculateMouseCoord(e);
                distance.x = lastPos.x - mousePos.x;
                distance.y = lastPos.y - mousePos.y;
                drawOnMyCanvas(e);
            }
        }
        function on_mouseup(e){
            ink = false;
        }
        function drawOnMyCanvas(e){
            ctx.strokeStyle = colorLine;
            ctx.lineCap = 'round';
            ctx.lineJoin = 'round';
            ctx.lineWidth = lineWidth;
            ctx.fillStyle = colorFill;
             
            if(outil === "pinceau" && isStroke === "checked"){
                ctx.beginPath();
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                ctx.closePath();
                lastPos.x = mousePos.x;
                lastPos.y = mousePos.y;
            }
            if(outil === "chenille"){
                var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
                if(isFill === "checked"){
                    ctx.beginPath();
                    ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
                    ctx.fill();
                    ctx.closePath();
                }
                if(isStroke === "checked"){
                    ctx.beginPath();
                    ctx.arc(mousePos.x, mousePos.y, Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y)+(lineWidth/2)), Math.PI*2, false);
                    ctx.stroke();
                    ctx.closePath();
                }
                lastPos.x = mousePos.x;
                lastPos.y = mousePos.y;
            }
            if(outil === "cercle"){
                var i = Number(sauvegarde.length) -1;
                ctx.putImageData(sauvegarde[i], 0, 0)
                if(isFill === "checked"){
                    ctx.beginPath();
                    ctx.arc(lastPos.x,lastPos.y, Math.abs(distance.x), Math.PI*2, false);
                    ctx.fill();
                    ctx.closePath();
                }
                if(isStroke === "checked"){
                    ctx.beginPath();
                    ctx.arc(lastPos.x,lastPos.y, (Math.abs(distance.x)+(lineWidth/2)), Math.PI*2, false);
                    ctx.stroke();
                    ctx.closePath();
                }
            }
            if(outil === "rectangle"){
                var i = Number(sauvegarde.length) -1;
                ctx.putImageData(sauvegarde[i], 0, 0);
                if(touchPressed === 16){distance.y = distance.x};
                if(isFill === "checked"){
                    ctx.beginPath();
                    ctx.rect(lastPos.x,lastPos.y, distance.x*-1, distance.y*-1);
                    ctx.fill();
                    ctx.closePath();
                }
 
                //bad
/*
                if(isStroke === "checked"){
                    ctx.beginPath();
                    ctx.rect(lastPos.x-(lineWidth/2),lastPos.y-(lineWidth/2), (distance.x-lineWidth)*-1, (distance.y-lineWidth)*-1);
                    ctx.stroke();
                    ctx.closePath();
                }
*/
                //bad - end
 
                //good
                 
                if(isStroke === "checked"){
                    var decalage = {x:lineWidth, y:lineWidth}
                    if(distance.x > 0){
                        decalage.x = -decalage.x;
                    }
                    if(distance.y > 0){
                        decalage.y = -decalage.y;
                    }
                    ctx.beginPath();
                    ctx.rect(lastPos.x-(decalage.x/2),lastPos.y-(decalage.y/2), distance.x*-1 + decalage.x, distance.y *-1 + decalage.y);
                    ctx.stroke();
                    ctx.closePath();
                }
                 
                //good - end
            }
        }
    }
{% endhighlight %}
