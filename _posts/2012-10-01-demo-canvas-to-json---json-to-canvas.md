---
layout: post
title: "Demo canvas to json - json to canvas"
description: ""
category: demo
date: 2011-08-05 19:43:00
scripts: ["/assets/js/experiences/canvastojson/exemple.js"]
styles: ["/assets/js/experiences/canvastojson/mystyle.css"]
tags: []
---

<div id="stuff" class="clearfix">
  <div id="toolbar">
    <a class="couleur active" color="#E91E79" style="background:#E91E79;"> </a>
    <a class="couleur" color="#ED5A24" style="background:#ED5A24;"> </a>
    <a class="couleur" color="#8F278B" style="background:#8F278B;"> </a>
    <a class="couleur" color="#88C23F" style="background:#88C23F;"> </a>
    <a class="couleur" color="#29A7DE" style="background:#29A7DE;"> </a>
    <a class="erase">X</a>
  </div>
  <textarea id="textarea"> </textarea>
</div>
<div id="surfacedetravail" style="position:relative">
  <div id="position-tracker" style="position:absolute; width: 840px; height: 223px; z-index: 100;"> </div>
  <canvas id="canvas-draw" style="position:absolute; z-index: 50;"> </canvas>
  <canvas id="canvas-bg" style="position:absolute; z-index: 10;"> </canvas>
</div>
