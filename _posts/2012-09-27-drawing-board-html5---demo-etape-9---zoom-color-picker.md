---
layout: post
title: "Drawing Board HTML5 - Démo étape 9 - Zoom et color picker"
description: ""
category: demo
promote: "true"
date: 2011-08-01 12:47:00
tags: []
scripts: ["/assets/js/experiences/drawing_board/partie9.js"]
---
<div>
  <div id="application" style="height:o;">
    <div id="position-tracker" style="position:absolute; z-index: 100;display:block; width: 640px; height: 480px;"> </div>
    <canvas id="picker-layer" style="position:absolute;">Your Browser Sucks</canvas>
    <canvas id="canvas">Your Browser Sucks</canvas>
  </div>
  <div class="colorPickerValue" id="colorResult">  </div>
</div>
