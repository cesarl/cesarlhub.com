---
layout: post
title: "Drawing Board HTML5 - Démo étape 7 - Contours, couleur et transparence"
description: ""
category: demo
tags: []
date: 2011-07-28 19:47:00
scripts: ["/assets/js/experiences/drawing_board/html5slider.js", "/assets/js/experiences/drawing_board/jscolor/jscolor.js", "/assets/js/experiences/drawing_board/partie7.js"]
styles: ["/assets/js/experiences/drawing_board/db_css.css"]
---
<div>
  <div id="toolbar">
    <a class="pinceau tool bouton" toolstype="pinceau"> </a>
    <a class="chenille tool bouton" toolstype="chenille"> </a>
    <a class="cercle tool bouton" toolstype="cercle"> </a>
    <a class="rectangle tool bouton" toolstype="rectangle"> </a>
    <a class="sauvegarde action bouton" toolstype="sauvegarde"> </a>
    <a class="historique action bouton" toolstype="historique"> </a>
  </div>
  <canvas id="canvas">Your Browser Sucks</canvas>
</div>

<div>
  <div id="proprietes">
    <canvas id="apercu-proprietes" width="200" height="140">Your browser sucks</canvas>
    <label for="fill">Fill : </label>
    <input type="checkbox" id="fill" checked="checked" />
    <div class="prop-box" parent="fill" id="fill-box">
      <div class="regles">
    	<input class="color" value="256BBA" id="fill-color" /><br>
    	<label for="opacity-fill">Opacité</label><br>
    	<input type="range" max="100" min="0" value="50" id="opacity-fill" class="range opacity" />
      </div>
    </div>
    <label for="stroke">Stroke : </label>
    <input type="checkbox" id="stroke" checked="checked" />
  <div class="prop-box" parent="stroke" id="stroke-box">
    <div class="regles">
      <input class="color" value="BABA12" id="stroke-color" /><br />
      <label for="opacity-stroke">Opacité</label><br />
      <input type="range" max="100" min="0" value="70" id="opacity-stroke" class="range opacity" />
      <label for="stroke-width">Epaisseur</label><br />
      <input type="range" max="30" min="1" value="30" id="stroke-width" class="range width" />
    </div>
  </div>
  </div>
</div>
