---
layout: post
title: "Outil rectangle et carre (partie 6)"
description: ""
category: post
tags: []
date: 2011-07-26 19:47:00
permalink: /outil-rectangle-et-carre-partie-6
illustration: "/images/dramatic-dog.jpg"
attachment: "Drawing Board HTML5 - Démo étape 6 - rectangle et carré"
---
{% include JB/setup %}

Et maintenant le rectangle et le carré !

Bon, c'est pas bien compliqué maintenant qu'on a vu le cercle. C'est à peu près pareil, il n'y a qu'à lire le code.

Seule subtilité, on y a ajouté un eventListener pour sur le keydown de manière à savoir si la touche Maj est enfoncée. Si elle l'est, alors on force la hauteur du rectangle à être égale à sa largeur, du coup, ça nous fait un carré. Cooool !

Vous pouvez essayer la démo ci-dessus.

Comme je vous disais en intro de cette série de post, je découvre quasi en même temps que vous, et je suis loin d'être un dieu du js, donc si vous voyez des améliorations, des bugs ou je ne sais quoi, n'hésitez pas, maillez-moi.

{% highlight javascript %}
$(document).ready(function(){
  
        var width = 910;
        var height = 561;
        var canvas = document.getElementById("canvas");
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext('2d');
        var lastPos = {x:0, y:0};
        var mousePos = {x:0, y:0};
        var mouseDiff = {x:0, y:0};
        var ink = false;
        var colorLine = "#BABA12"
        var lineWidth = 10
        var myImage = new Image();
        myImage.src = "/images/ash2.jpg";
        myImage.addEventListener('load', imageIsLoaded, false);
        var letterPressed;
        var touchPressed;
        var sauvegarde = [];
        var outil = "pinceau";
        var action = "";
        var distance = {x:10, y:10};
        var lastCircleRadius;
        $("#toolbar").find("."+outil).addClass("active");
          
        function imageIsLoaded(){
            ctx.drawImage(myImage, 0,0);
  
            canvas.addEventListener("mousedown", on_mousedown, false);
            canvas.addEventListener("mousemove", on_mousemove, false);
            canvas.addEventListener("mouseup", on_mouseup, false);
            window.addEventListener("keyup",oneKeyUp,true);
            window.addEventListener("keydown",oneKeyDown,true);
  
        $("#toolbar .bouton").click(function(){
            if($(this).hasClass("tool")){
                $(this).addClass("active")
                $(".tool").not(this).removeClass("active");
                outil = $(this).attr("toolstype");
            };
            if($(this).hasClass("action")){
                action = $(this).attr("toolstype");
                actions();
            }
        });
        }
        function oneKeyUp(e){
            letterPressed = String.fromCharCode(e.keyCode);
            touchPressed = "";
            actions(e);
        }
        function oneKeyDown(e){
            touchPressed = e.keyCode;
        }
        function actions(){
            if(action === "historique" || letterPressed === "Z"){
                var i = Number(sauvegarde.length);
                if(i>0){
                    ctx.putImageData(sauvegarde.pop(), 0, 0);
                }
            }
          
            if(action === "sauvegarde" || letterPressed === "S"){
                var canvasToImg = canvas.toDataURL();
                window.open(canvasToImg,"nom de la fenêtre","left=0,top=0,width=" + canvas.width + ",height=" + canvas.height +",toolbar=no,resizable=no, directories=no, location=no, status = no, ");
            }
            action ="";
        }
  
        function scrollPosition(e) {
            return {
                x: document.scrollLeft || window.pageXOffset,
                y: document.scrollTop || window.pageYOffset
            };
        }
        function calculateMouseCoord(e){
            return{
                x: e.clientX - canvas.offsetLeft + scrollPosition().x,
                y: e.clientY - canvas.offsetTop + scrollPosition().y
            }
        }
        function on_mousedown(e){
        sauvegarde.push(ctx.getImageData(0,0,width,height))
            ink = true;
            lastPos = calculateMouseCoord(e);
        }
        function on_mousemove(e){
            if(ink == false){return false} 
            else{
                mousePos = calculateMouseCoord(e);
                distance.x = lastPos.x - mousePos.x;
                distance.y = lastPos.y - mousePos.y;
                drawOnMyCanvas(e);
            }
        }
        function on_mouseup(e){
            ink = false;
        }
        function drawOnMyCanvas(e){
            if(outil === "pinceau"){
                ctx.beginPath();
                ctx.strokeStyle = colorLine;
                ctx.lineWidth = lineWidth;
                ctx.lineCap = 'round';
                ctx.lineJoin = 'round';
                ctx.moveTo(lastPos.x, lastPos.y);
                ctx.lineTo(mousePos.x, mousePos.y);
                ctx.stroke();
                ctx.closePath();
                lastPos.x = mousePos.x;
                lastPos.y = mousePos.y;
            }
            if(outil === "chenille"){
                var rayon = Math.abs((mousePos.x + mousePos.y)-(lastPos.x + lastPos.y));
                ctx.beginPath();
                ctx.fillStyle = "#D0145A";
                ctx.arc(mousePos.x, mousePos.y, rayon, Math.PI*2, false);
                ctx.fill();
                ctx.closePath();
                lastPos.x = mousePos.x;
                lastPos.y = mousePos.y;
            }
            if(outil === "cercle"){
                var i = Number(sauvegarde.length) -1;
                ctx.putImageData(sauvegarde[i], 0, 0)
                ctx.strokeStyle = colorLine;
                ctx.strokeWidth = lineWidth;
                ctx.fillStyle = "#D0145A";
                ctx.beginPath();
                ctx.arc(lastPos.x,lastPos.y, Math.abs(distance.x), Math.PI*2, false);
                ctx.fill();
                ctx.closePath();
            }
            if(outil === "rectangle"){
                var i = Number(sauvegarde.length) -1;
                ctx.putImageData(sauvegarde[i], 0, 0)
                ctx.strokeStyle = colorLine;
                ctx.strokeWidth = lineWidth;
                ctx.fillStyle = "#D0145A";
                if(touchPressed === 16){distance.y = distance.x};
                ctx.beginPath();
                ctx.rect(lastPos.x,lastPos.y, distance.x*-1, distance.y*-1);
                ctx.fill();
                ctx.closePath();
            }
        }
});
{% endhighlight %}