---
layout: post
title: "Curriculum Vitae 2012"
description: "Curriculum Vitae César LEBLIC 2012"
category: demo
promote: "true"
scripts: ["/assets/js/cv2012/js/animationFrame.js", "/assets/js/cv2012/js/pubsub.js", "/assets/js/cv2012/js/app.js", "/assets/js/cv2012/js/main.js"]
illustration: "/images/cv_2012_thumb.png"
tags: []
---
{% include JB/setup %}

<canvas id="canvas"></canvas>
