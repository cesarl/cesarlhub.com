---
layout: post
title: "Sauvegarde de l'image - Canvas to Img (partie 3)"
description: ""
category: post
tags: []
date: 2011-07-25 19:00:00
permalink: /ajouter-un-historique-partie-2
illustration: "/images/sheep.png"
attachment: "Drawing Board HTML5 - Démo étape 3 - sauvegarde"
---
{% include JB/setup %}

Nous allons ajouter une fonctionnalité toute simple , mais essentielle à notre drawing board, l'enregistrement.

Pour le moment nous allons rester très basique et nous contenter de convertir notre dessin en image.

Encore une fois, nous reprenons le code de la partie précédente, et encore une fois vous trouverez une démo ci-dessus.

Nous allons renommer l'eventListener ctrlz vu dans la partie 2 de cette série d'articles en oneKeyUp, plus général.

{% highlight javascript %}
window.addEventListener("keyup",oneKeyUp,true);
{% endhighlight %}

Nous allons modifier notre ancienne fonction ctrlz, renommée oneKeyUp pour qu'elle puisse traiter l'historique et la sauvegarde, en y ajoutant une nouvelle conditionnelle, cette fois pour la lettre "S"

- Si la touche appuyée est égale à "S" if(letterPressed === "S")
- Alors nous définissons une variable que l'on appelle canvasToImg. Cette dernière sera égale au contexte actuel du canvas converti en data URI, c'est-à-dire en image sous la forme de bits en base 64 ... bref pour faire simple c'est comme une image en code. [En savoir plus](http://en.wikipedia.org/wiki/Data_URI_scheme)
- Puis nous ouvrons une nouvelle fenêtre avec à l'intérieur, notre belle image ! [En savoir plus sur window.open](http://www.commentcamarche.net/contents/javascript/jswindow.php3)

Essayez, avec la démo ci dessous, dessinez, appuyez sur la touche S et hop, vous pouvez enregistrer-sous votre beau dessins

{% highlight javascript %}
function oneKeyUp(e){
//tout ça, on connait déjà
    var letterPressed = String.fromCharCode(e.keyCode);
    if(letterPressed === "Z"){
        var i = Number(sauvegarde.length);
        if(i>0){
            ctx.putImageData(sauvegarde.pop(), 0, 0);
        }
    }

    //si la touche appuyée est égale à "S"
    if(letterPressed === "S"){
        //nous convertissons le canvas en data Url
        // in English : returns a data URL containing a base64 encoding of the raw image bytes - yeah !!!
        var canvasToImg = canvas.toDataURL();
        //on ouvre une nouvelle fenêtre avec notre beau dessin
        //que l'on peut enregistrer-sous
        //coooooooooooooooool
        window.open(canvasToImg,"nom de la fenêtre", "left=0, top=0, width=" + canvas.width + ", height=" + canvas.height +", toolbar=no, resizable=no, directories=no, location=no, status = no, ");
    }
}
{% endhighlight %}
