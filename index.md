---
layout: page
id: front
---
{% include JB/setup %}
{% include JB/setup %}

## Derniers posts

<div class="posts promotion-list">
  {% for post in site.posts %}
    {% if post.category == "post" %}
      <div class="promoted post clearfix">
        <div class="field-created">
          <span>{{ post.date | date_to_string }}</span>
        </div>
        {% if post.illustration != null %}
          <div class="field-illustration clearfix">
            <a href="{{ BASE_PATH }}{{ post.url }}" class="clearfix"><img src="{{ post.illustration }}" class="img img-illustration clearfix" /></a>
          </div>
        {% endif %}
        <div class="field-title">
          <a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a>
        </div>
        <div class="field-body">
          {{ post.content | truncate: 430 | strip_html}}
        </div>
      </div>
    {% endif %}
  {% endfor %}
</div>


