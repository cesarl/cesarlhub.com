---
layout: page
title: "Contact"
description: ""
weight: 5
no-title: true
---
{% include JB/setup %}

## cesarleblic(at)gmail(dot)com
{:.colored .no-underline .centered}
## [Linkedin](http://www.linkedin.com/pub/c%C3%A9sar-leblic/40/198/192)
{:.colored1 .no-underline .centered}
## [Github](https://github.com/cesarl)
{:.colored2 .no-underline .centered}
## [Bitbucket](https://bitbucket.org/cesarl)
{:.colored3 .no-underline .centered}
## [Code School](http://www.codeschool.com/users/164150)
{:.colored .no-underline .centered}
## [Coursera](https://www.coursera.org/user/i/3b0acf39a542086148757bad3795c098)
{:.colored1 .no-underline .centered}
## [MeetUp](http://www.meetup.com/member/32312302/)
{:.colored2 .no-underline .centered}
## [Twitter](https://twitter.com/cesarleblic)
{:.colored3 .no-underline .centered}
## [Facebook](https://www.facebook.com/cesar.leblic)
{:.colored .no-underline .centered}
