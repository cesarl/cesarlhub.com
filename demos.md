---
layout: page
title: "Démos"
description: ""
weight: 8
no-title: true
id: front
---
{% include JB/setup %}

<div class="demo demo-promotion-list promotion-list">
  {% for post in site.posts %}
    {% if post.category == "demo" %}
      <div class="promoted post clearfix">
        <div class="field-created">
          <span>{{ post.date | date_to_string }}</span>
        </div>
        {% if post.illustration != null %}
          <div class="field-illustration clearfix">
            <a href="{{ BASE_PATH }}{{ post.url }}" class="clearfix"><img src="{{ post.illustration }}" class="img img-illustration clearfix" /></a>
          </div>
        {% endif %}
        <div class="field-title">
          <a href="{{ BASE_PATH }}{{ post.url }}">{{ post.title }}</a>
        </div>
      </div>
    {% endif %}
  {% endfor %}
</div>
